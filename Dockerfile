FROM londotech/node-ts:latest

USER root

# Expose the port the app runs in
EXPOSE 4002

ENV NODE_PATH=/usr/lib/node_modules

COPY package.json /tmp/package.json

RUN cd /tmp && npm install --only=production --unsafe-perm=true

RUN mkdir -p /usr/src/credits && cp -a /tmp/node_modules /usr/src/credits/

WORKDIR /usr/src/credits

COPY . /usr/src/credits

# RUN NODE_ENV=staging-bci
CMD npm run start:staging-bci