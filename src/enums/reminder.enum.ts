export enum ReminderType {
    BEFORE_DEADLINE = 1,
    UNPAID = 2,
    FIRST_REMINDER = 3,
    SECOND_REMINDER = 4
}