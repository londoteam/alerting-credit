export enum OperationCode {
    SALARY = 100,
    PRET = 101,
    DEBIT = 102,
    LOAN = 103,
    MINI_STATEMENT = 'MINI_STATEMENT',
    CHECKBOOK_AVAILABILITY = 105,
    CREDIT = 'CREDITS',
    INSTANT_NOTICATION = 'INSTANT',
}