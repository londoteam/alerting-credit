import { Db, InsertWriteOpResult } from 'mongodb';
import { getDatabase } from './config';
import { helper } from './helpers/payment-dues.helper'
import { get } from 'lodash';

const collectionName = 'payment-dues';

export const paymentDuesCollection = {

    insertPaymentDues: async (paymentDues: any[]): Promise<InsertWriteOpResult<any>> => {
        const database: Db = await getDatabase();
        return await database.collection(collectionName).insertMany(paymentDues);
    },

    getPaymentDues: async (fields: any): Promise<any[]> => {
        const database = await getDatabase();
        let query: any = [];
        if (fields.dueDate) {
            const { start, end } = fields.dueDate;
            query = [{ $match: { DATE_APPEL: { $gte: start, $lte: end } } }];
        }

        if (fields.clientProfile) {
            query.push({ $match: { PROFIL_CLIENT: fields.clientProfile } });
        }

        return await database.collection(collectionName).aggregate(query).toArray();
    },

    getPaymentDuesStatistics: async (start?: any, end?: any): Promise<any[]> => {
        const database = await getDatabase();
        let query: any = [];

        if (start && end) {
            query = [{ $match: { DATE_APPEL: { $gte: start, $lte: end } } }];
        }

        query = query.concat(
            [
                { $match: { STATUT: 'IMPAYE' } },
                { $group: { _id: true, nbEch: { $sum: 1 }, total: { $sum: '$TOTAL_ECH' } } },
                { $project: { _id: 0 } }
            ]
        );

        const totalQuery = (start && end) ? { DATE_APPEL: { $gte: start, $lte: end } } : {}
        const total = await database.collection(collectionName).find(totalQuery).count();
        const data = await database.collection(collectionName).aggregate(query).toArray();

        let unpaidRate = 0;
        if (data.length > 0) {
            unpaidRate = parseFloat(((data[0].nbEch / total) * 100).toFixed(2));
            data[0].unpaidRate = unpaidRate || 0;
        }


        return data;
    },

    getNotificationsPaymentDuesStatistics: async (start?: any, end?: any): Promise<any[]> => {
        const database = await getDatabase();
        let query: any = [];

        if (start && end) {
            query = [{ $match: { DATE_APPEL: { $gte: start, $lte: end } } }];
        }

        query = query.concat(
            [
                { $match: { STATUT: { $ne: 'PAYE' } } },
                { $group: { _id: true, nbEch: { $sum: 1 }, total: { $sum: '$TOTAL_ECH' } } },
                { $project: { _id: 0 } }
            ]
        );

        const totalQuery = (start && end) ? { DATE_APPEL: { $gte: start, $lte: end } } : {}

        const total = await database.collection('payment-dues-notification').find(totalQuery).count();

        const data = await database.collection('payment-dues-notification').aggregate(query).toArray();

        const result = data[0] || { nbEch: 0, total: 0 };
        result.unpaidRate = parseFloat(((get(data[0], 'nbEch', 0) / total) * 100).toFixed(2)) || 0;
        return result;
    },

    getUsersContactStatus: async (start: any, end: any): Promise<any> => {
        const database = await getDatabase();
        const query: any = helper.generateUsersContactStatusQuery();
        return await database.collection(collectionName).aggregate(query).toArray();
    },

    dropCollection: async (): Promise<any> => {
        const database: Db = await getDatabase();
        try {
            await database.dropCollection(collectionName);
        } catch (error) { }
    }
}