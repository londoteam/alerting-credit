import { InsertOneWriteOpResult, ObjectID } from 'mongodb';
import { TemplateForm } from '../models/template';
import { TemplateQuery } from '../models/template-query';
import { getDatabase } from './config';

const collectionName = 'templates_credits';

export const templatesCollection = {

    insertTemplate: async (template: TemplateForm): Promise<InsertOneWriteOpResult<any>> => {
        const database = await getDatabase();
        template.enabled = true;
        return await database.collection(collectionName).insertOne(template);
    },

    getTemplates: async (filters: any): Promise<TemplateQuery[]> => {
        const database = await getDatabase();
        return await database.collection(collectionName).find(filters).toArray();
    },

    getTemplateBy: async (filters: any): Promise<TemplateForm> => {
        const database = await getDatabase();
        if (filters._id) { filters._id = new ObjectID(filters._id) }
        return await database.collection(collectionName).findOne(filters);
    },

    updateTemplateById: async (id: string, template: TemplateForm) => {
        const database = await getDatabase();
        delete template._id;
        return await database.collection(collectionName).updateOne(
            { _id: new ObjectID(id), },
            { $set: { ...template } }
        );
    }

}