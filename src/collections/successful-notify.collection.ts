import { ObjectID, UpdateWriteOpResult } from 'mongodb';
import { getDatabase } from './config';

const collectionName = 'successful-notifications';

export const successfulNotifysCollection = {

    getSuccessfulNotications: async (filters: any, offset?: any, limit?: any, range?: any): Promise<any> => {

        offset = offset || 1;
        limit = limit || 40;

        const { format, queryFilter, status } = filters;

        let query: any = {};

        if (queryFilter) {
            query.$or = [{ clientCode: queryFilter }, { email: queryFilter }, { telephone: queryFilter }];
        }

        if (format) { query.format = format; }

        if (status) { query.status = status; }

        if (range) { query = { ...query, 'dates.sentAt': { $gte: range.start, $lte: range.end } }; }

        const startIndex = (offset - 1) * limit;

        const database = await getDatabase();

        const total = await database.collection(collectionName).find(query).count();

        const data = await database.collection(collectionName).find(query).sort({ "dates.sendAt": -1 }).skip(startIndex).limit(limit).toArray();
        return { data, total };
    },

    getSuccessfulNoticationsBy: async (filters: any): Promise<any> => {
        const database = await getDatabase();
        return await database.collection(collectionName).find({ ... filters }).sort({ "dates.sendAt": -1 }).toArray();
    },

    getSuccessfulNotificationBy: async (filters: any): Promise<any> => {
        const database = await getDatabase();
        if (filters._id) { filters._id = new ObjectID(filters._id) }
        return await database.collection(collectionName).findOne(filters);
    },

    updateSuccessfulNotificationById: async (id: string, notification: any): Promise<UpdateWriteOpResult> => {
        const database = await getDatabase();
        delete notification._id;
        return await database.collection(collectionName).updateOne(
            { _id: new ObjectID(id), },
            { $set: { ...notification } }
        );
    },

    deleteSuccessfulNotificationById: async(id: string) => {
        const database = await getDatabase();
        try {
            return await database.collection(collectionName).deleteOne({
                _id: new ObjectID(id)
            });
        } catch (error) {
            return null;
        }
    }

}