import { logger } from './../winston';
import { InsertOneWriteOpResult, InsertWriteOpResult, ObjectID, UpdateWriteOpResult } from 'mongodb';
import { getDatabase } from './config';
import { isEmpty } from 'lodash';

const collectionName = 'notifications';

export const notifysCollection = {

    insertCreditsNotification: async (notifications: any[]): Promise<InsertOneWriteOpResult<any>> => {
        const database = await getDatabase();
        if (!notifications || notifications.length === 0) { return; }
        return await database.collection('credits').insertOne(notifications);
    },

    insertCreditsNotifications: async (notifications: any[]): Promise<InsertWriteOpResult<any>> => {
        const database = await getDatabase();
        if (!notifications || notifications.length === 0) { return; }
        return await database.collection('credits').insertMany(notifications);
    },

    getAllCreditsNotications: async (): Promise<any[]> => {
        // logger.info(`Get all credits notifications in credits collection`);
        const database = await getDatabase();
        return await database.collection('credits').find({}).sort({ _id: -1 }).toArray();
    },

    deleteCreditsNotificationById: async (id: any): Promise<any> => {
        if (!id) { return; }
        const database = await getDatabase();
        return database.collection('credits').deleteOne({_id: new ObjectID(id.toString())});
    },

    insertSendingNotification: async (notification: any): Promise<InsertOneWriteOpResult<any>> => {
        const database = await getDatabase();
        if (!notification) { return; }
        return await database.collection(collectionName).insertOne(notification);
    },

    insertSendingNotifications: async (notifications: any[]): Promise<InsertWriteOpResult<any>> => {
        const database = await getDatabase();
        if (!notifications || notifications.length === 0) { return; }
        return await database.collection(collectionName).insertMany(notifications);
    },

    insertSingleNotification: async (notification: any): Promise<InsertOneWriteOpResult<any>> => {
        const database = await getDatabase();
        if (!notification) { return; }
        return await database.collection(collectionName).insertOne(notification);
    },

    getTotalEntries: async (collection: string, filters?: any): Promise<number> => {
        const database = await getDatabase();
        let total: number;
        (filters)
            ? total = await database.collection(collection).find({ ...filters }).count()
            : total = await database.collection(collection).find({}).count();
        return total;
    },

    getNoticationsWithNoLimit: async (filters: any, range?: any, startIndex?: any, limit?: any): Promise<any> => {

        logger.info(`Get notifications with startIndex: ${startIndex} and limit :${limit}; filters \n${JSON.stringify(filters)}`);

        const { format, status } = filters;

        const query: any = {};

        if (format) { query.format = format; }

        if (status >= 0) { query.status = status; }

        if (range) { query.createdAt = { $gte: range.start, $lte: range.end }; }

        const database = await getDatabase();

        const data = (isEmpty(startIndex) && isEmpty(limit))
            ? await database.collection(collectionName).find(query).sort({ createdAt: -1 }).skip(startIndex).limit(limit).toArray()
            : await database.collection(collectionName).find(query).sort({ createdAt: -1 }).limit(200).toArray();

        return data;
    },

    updateNotificationById: async (id: string, notification: any): Promise<UpdateWriteOpResult> => {
        const database = await getDatabase();
        delete notification._id;
        return await database.collection(collectionName).updateOne(
            { _id: new ObjectID(id), },
            { $set: { ...notification } }
        );
    },

}