import { InsertOneWriteOpResult, InsertWriteOpResult, ObjectID, UpdateWriteOpResult } from 'mongodb';
import { getDatabase } from './config';

const collectionName = 'regions';

export const regionsCollection = {

    insertRegion: async (region: any): Promise<InsertOneWriteOpResult<any>> => {
        const database = await getDatabase();
        return await database.collection(collectionName).insertOne(region);
    },

    getRegions: async (filters: any): Promise<any> => {

        const query: any = { ...filters };
        const database = await getDatabase();
        return await database.collection(collectionName).find(query).sort({ label: -1 }).toArray();
    },

    getRegionBy: async (filters: any): Promise<any> => {
        const database = await getDatabase();
        if (filters._id) { filters._id = new ObjectID(filters._id) }
        return await database.collection(collectionName).findOne(filters);
    },

    updateRegionById: async (id: string, region: any): Promise<UpdateWriteOpResult> => {
        const database = await getDatabase();
        delete region._id;
        return await database.collection(collectionName).updateOne(
            { _id: new ObjectID(id), },
            { $set: { ...region } }
        );
    },

    getRegionListReporting: async (fields?: any): Promise<any> => {
        const database = await getDatabase();
        return await database.collection(collectionName).find({ code: { $not: { $eq: 'SIEGE' } } }).toArray();
    },

}