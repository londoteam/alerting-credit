import { Db, InsertWriteOpResult } from 'mongodb';
import { getDatabase } from './config';
import * as moment from 'moment';
import { logger } from '../winston';

const collectionName = 'payment-dues-notification';

export const notificationPaymentDuesCollection = {

    insertNotificationPaymentDues: async (paymentDues: any[]): Promise<InsertWriteOpResult<any>> => {
        const database: Db = await getDatabase();
        return await database.collection(collectionName).insertMany(paymentDues);
    },

    getNotificationPaymentDues: async (fields: any): Promise<any[]> => {
        const database = await getDatabase();
        let { start, end, clientProfile, status } = fields;
        const query: any = {};

        if (start && end) { query.DATE_APPEL = { $gte: start, $lte: end }; }

        if (clientProfile) { query.CODE_PROFIL_CLIENT = clientProfile; }

        if (status) {
            status = 100 ? { $in: ['IMPAYE', 'DUE', ''] } : 'PAYE'
            query.STATUT = status;
        }

        logger.info(`Get payment dues query: ${JSON.stringify(query)}`);

        const data =  await database.collection(collectionName).find(query).toArray();
        return data;
    },

    getNotificationPaymentDuesStatistics: async (start?: any, end?: any): Promise<any[]> => {
        const database = await getDatabase();
        let query: any = [];

        if (start && end) {
            query = [{ $match: { DATE_APPEL: { $gte: start, $lte: end } } }];
        }

        query = query.concat(
            [
                { $match: { STATUT: 'IMPAYE' } },
                { $group: { _id: true, nbEch: { $sum: 1 }, total: { $sum: '$TOTAL_ECH' } } },
                { $project: { _id: 0 } }
            ]
        );

        return await database.collection(collectionName).aggregate(query).toArray();
    },

    dropCollection: async (): Promise<any> => {
        const database: Db = await getDatabase();
        try {
            await database.dropCollection(collectionName);
        } catch (error) { }
    }
}