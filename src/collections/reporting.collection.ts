import { getDatabase } from './config';
import { helper } from './helpers/cae-reporting.helper';

const collectionName = 'payment-dues';

export const reportingsCollection = {

    getCaesReporting: async (): Promise<any> => {
        const database = await getDatabase();
        const totalEchQuery: any = helper.generateEchQuery();
        return await database.collection(collectionName).aggregate(totalEchQuery).toArray();
    },

    getCaesRegionList: async (): Promise<any> => {
        const database = await getDatabase();
        const totalEchQuery: any = helper.generateCaesRegionListQuery();
        return await database.collection(collectionName).aggregate(totalEchQuery).toArray();
    },

}