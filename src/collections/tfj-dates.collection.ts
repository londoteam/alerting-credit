import { InsertOneWriteOpResult, InsertWriteOpResult, ObjectID, UpdateWriteOpResult } from 'mongodb';
import { getDatabase } from './config';

const collectionName = 'tfj';

export const tfjDatesCollection = {

    insertTfjDate: async (tfjDate: any): Promise<InsertOneWriteOpResult<any>> => {
        const database = await getDatabase();
        return await database.collection(collectionName).insertOne(tfjDate);
    },

    getLastTfjDate: async (): Promise<any> => {
        const database = await getDatabase();
        return await database.collection(collectionName).findOne({});
    },

    getTfjDateBy: async (filters: any): Promise<any> => {
        const database = await getDatabase();
        if (filters._id) { filters._id = new ObjectID(filters._id) }
        return await database.collection(collectionName).findOne(filters);
    },

    updateLastTfjDate: async (tfjDate: any): Promise<UpdateWriteOpResult> => {
        const database = await getDatabase();
        return await database.collection(collectionName).updateOne({}, { $set: { lastTfjDate: tfjDate } });
    },

}