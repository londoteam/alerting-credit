import { ObjectID, UpdateWriteOpResult } from 'mongodb';
import { config } from '../config';
import { getDatabase } from './config';
import { helper } from './helpers/statistics.helper'

const collectionName = 'unreachable';

export const unreachablesClientCollection = {

    getAggrateUnreachableClients: async (): Promise<any> => {
        const database = await getDatabase();
        let query: any;
        if (config.get('env') === 'staging-bicec' || config.get('env') === 'prodcution') {
            query = helper.generateUnreachableUserQuery();
        } else {
            query = helper.generateUnreachableUserQuery2();
        }
        return await database.collection('payment-dues-notification').aggregate(query).toArray();
    },

    insertUnreachables: async (unreachables: any): Promise<any> => {
        const database = await getDatabase();
        if (unreachables.length === 0) { return; }
        unreachablesClientCollection.dropCollection();
        return await database.collection(collectionName).insertMany(unreachables);
    },

    getUnreachables: async (filters: any, offset?: any, limit?: any): Promise<any> => {

        offset = offset || 1;
        limit = limit || 40;

        const { type, clientProfile, code } = filters;
        const query: any = {};

        if (type && (config.get('env') === 'staging-bicec' || config.get('env') === 'prodcution')) {
            query.$or = (type === 100) ? [{ phone: null }, { phone: '' }] : [{ email: null }, { email: '' }];
        } else {
            query.$or = (type === 100) ? [{ phone: { $exists: false } }, { phone: 'null' }] : [{ email: { $exists: false } }, { email: 'null' }];
        }
        if (clientProfile) {
            query.profil = (clientProfile === 100) ? '600' : { $ne: '600' };
        }
        if (code) { query.code = code.toString(); }

        const startIndex = (offset - 1) * limit;
        const database = await getDatabase();

        const total = await database.collection(collectionName).find(query).count();
        const data = await database.collection(collectionName).find(query).sort({ nom_client: -1 }).skip(startIndex).limit(limit).toArray();

        return { data, total };
    },

    getUnreachableBy: async (filters: any): Promise<any> => {
        const database = await getDatabase();
        if (filters._id) { filters._id = new ObjectID(filters._id) }
        return await database.collection(collectionName).findOne(filters);
    },

    updateUnreachableById: async (id: string, unreachable: any): Promise<UpdateWriteOpResult> => {
        const database = await getDatabase();
        delete unreachable._id;
        return await database.collection(collectionName).updateOne(
            { _id: new ObjectID(id), },
            { $set: { ...unreachable } }
        );
    },

    dropCollection: async (): Promise<any> => {
        const database = await getDatabase();
        try {
            await database.dropCollection(collectionName);
        } catch (error) { }
    }

}