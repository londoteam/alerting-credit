
import { ObjectID } from 'mongodb';
import { getDatabase } from './config';

const collectionName = 'successful-credit-notifications';

export const successfulCreditNotifysCollection = {

    getSuccessfulCreditNotificationBy: async (filters: any): Promise<any> => {
        const database = await getDatabase();
        if (filters._id) { filters._id = new ObjectID(filters._id) }
        return await database.collection(collectionName).findOne(filters);
    },

}