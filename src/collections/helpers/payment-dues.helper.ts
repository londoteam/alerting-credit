import * as moment from 'moment';
moment.locale('fr');

export const helper = {

    generateUsersContactStatusQuery: () => {

        const query = [
            {

                $facet: {
                    nbUsersWthPhone: [
                        { $match: { TELEPHONE_CLIENT: { $exists: false } } },
                        { $group: { _id: true, nbelmt: { $sum: 1 } } },
                        { $project: { _id: 0 } }
                    ],
                    nbUsersWthEmail: [
                        { $match: { EMAIL_CLIENT: { $exists: false } } },
                        { $group: { _id: true, nbelmt: { $sum: 1 } } },
                        { $project: { _id: 0 } }
                    ],
                    nbUsersWthPhoneAndEmail: [
                        { $match: { $and: [{ TELEPHONE_CLIENT: { $exists: false } }, { EMAIL_CLIENT: { $exists: false } }] } },
                        { $group: { _id: true, nbemlt: { "$sum": 1 } } },
                        { $project: { _id: 0 } }
                    ],

                }

            }
        ];

        return query;
    },

}
