import { logger } from '../../winston';
import * as moment from 'moment';
import { get } from 'lodash';
moment.locale('fr');

export const helper = {

    generateEchQuery: () => {
        const query = [
            {
                $facet: {
                    totalEch: [
                        {
                            $group: {
                                _id: {
                                    code: "$CODE_GESTIONNAIRE",
                                    email: "$EMAIL_GESTIONNAIRE",
                                    nom: "$NOM_GESTIONNAIRE",
                                    age: "$CODE_AGENCE"
                                },

                                totalEch: { $sum: "$TOTAL_ECH" },
                                nbEch: { $sum: 1 },
                            }
                        },
                        {
                            $project: {
                                _id: 0,
                                code: "$_id.code",
                                email: "$_id.email",
                                fullName: "$_id.nom",
                                age: "$_id.age",
                                nbEch: 1,
                                totalEch: 1
                            }
                        }
                    ],
                    totalUnpaidEch: [
                        { $match: { STATUT: 'IMPAYE' } },
                        {
                            $group: {
                                _id: {
                                    code: "$CODE_GESTIONNAIRE",
                                    email: "$EMAIL_GESTIONNAIRE",
                                    nom: "$NOM_GESTIONNAIRE",
                                    status: "$STATUT",
                                    age: "$CODE_AGENCE"
                                },
                                totalEch: { $sum: "$TOTAL_ECH" },
                                nbEch: { $sum: 1 }
                            }
                        },
                        {
                            $project: {
                                _id: 0,
                                code: "$_id.code",
                                email: "$_id.email",
                                nom: "$_id.nom",
                                status: "$_id.status",
                                age: "$_id.age",
                                nbEch: 1,
                                totalEch: 1
                            }
                        }
                    ],
                    totalpaidEch: [
                        { $match: { STATUT: "PAYE" } },
                        {
                            $group: {
                                _id: {
                                    code: "$CODE_GESTIONNAIRE",
                                    email: "$EMAIL_GESTIONNAIRE",
                                    nom: "$NOM_GESTIONNAIRE",
                                    status: "$STATUT",
                                    age: "$CODE_AGENCE"
                                },

                                totalEch: { $sum: "$TOTAL_ECH" },
                                nbEch: { $sum: 1 }
                            }
                        }, {
                            $project: {
                                _id: 0,
                                code: "$_id.code",
                                email: "$_id.email",
                                nom: "$_id.nom",
                                status: "$_id.status",
                                age: "$_id.age",
                                nbEch: 1,
                                totalEch: 1
                            }
                        }
                    ],
                    totalDueEch: [
                        { $match: { STATUT: 'DUE' } },
                        {
                            $group: {
                                _id: {
                                    code: "$CODE_GESTIONNAIRE",
                                    email: "$EMAIL_GESTIONNAIRE",
                                    nom: "$NOM_GESTIONNAIRE",
                                    status: "$STATUT",
                                    age: "$CODE_AGENCE"
                                },

                                totalEch: { $sum: "$TOTAL_ECH" },
                                nbEch: { $sum: 1 }
                            }
                        }, {
                            $project: {
                                _id: 0,
                                code: "$_id.code",
                                email: "$_id.email",
                                status: "$_id.status",
                                age: "$_id.age",
                                nbEch: 1,
                                totalEch: 1
                            }
                        }
                    ],
                    clientListUnpaidEch: [
                        { $match: { STATUT: { $in: [ 'IMPAYE', 'DUE', ''] } } },
                        {
                            $group: {
                                _id: {
                                    dateEch: "$DATE_APPEL",
                                    totalEch: "$TOTAL_ECH",
                                    clientCode: "$CODE_CLIENT",
                                    code: "$CODE_GESTIONNAIRE",
                                    email: "$EMAIL_GESTIONNAIRE",
                                    clientName: "$NOM_DU_CLIENT",
                                    clientEmail: "$EMAIL_CLIENT",
                                    clientTel: "$TELEPHONE_CLIENT",
                                    age: "$CODE_AGENCE"
                                }
                            }
                        },
                        {
                            $project: {
                                _id: 0,
                                code: "$_id.code",
                                email: "$_id.email",
                                dateEch: "$_id.dateEch",
                                totalEch: "$_id.totalEch",
                                clientTel: "$_id.clientTel",
                                clientName: "$_id.clientName",
                                clientCode: "$_id.clientCode",
                                clientEmail: "$_id.clientEmail",
                                age: "$_id.age",
                            }
                        }
                    ]
                }
            }
        ];
        return query;
    },

    generateCaesRegionListQuery: () => {
        const query = [
            {
                $facet: {
                    totalCae: [
                        {
                            $group: {
                                _id: {
                                    code: "$CODE_GESTIONNAIRE",
                                    email: "$EMAIL_GESTIONNAIRE",
                                    nom: "$NOM_GESTIONNAIRE"
                                }
                            }
                        },
                        {
                            $project: {
                                _id: 0,
                                code: "$_id.code",
                                email: "$_id.email",
                                nom: "$_id.nom"
                            }
                        }
                    ],
                    totalRMCae: [
                        { $match: { CODE_REGION: "DRM" } },
                        {
                            $group: {
                                _id: {
                                    code: "$CODE_GESTIONNAIRE",
                                    email: "$EMAIL_GESTIONNAIRE",
                                    nom: "$NOM_GESTIONNAIRE",
                                    rg: "$CODE_REGION"
                                }
                            }
                        },
                        {
                            $project: {
                                _id: 0,
                                code: "$_id.code",
                                email: "$_id.email",
                                nom: "$_id.nom",
                                rg: "$_id.rg"
                            }
                        }
                    ],
                    totalRCCae: [
                        { $match: { CODE_REGION: "DRC" } },
                        {
                            $group: {
                                _id: {
                                    code: "$CODE_GESTIONNAIRE",
                                    email: "$EMAIL_GESTIONNAIRE",
                                    nom: "$NOM_GESTIONNAIRE",
                                    rg: "$CODE_REGION"
                                }
                            }
                        },
                        {
                            $project: {
                                _id: 0,
                                code: "$_id.code",
                                email: "$_id.email",
                                nom: "$_id.nom",
                                rg: "$_id.rg"
                            }
                        }
                    ]
                }
            }
        ];
        return query;
    },

}
