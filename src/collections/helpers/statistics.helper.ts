import * as moment from 'moment';
moment.locale('fr');

export const helper = {

    generateNotifNumbersQuery: (start: any, end: any) => {
        let query = [];

        query = query.concat(
            [
                {
                    $facet: {
                        nbSms: [
                            { $match: { format: 100 } },
                            { $group: { _id: true, nb: { $sum: "$smsCount" } } },
                            { $project: { _id: 0, } }
                        ],
                        nbEmail: [
                            { $match: { format: 200 } },
                            { $group: { _id: true, nb: { $sum: 1 } } },
                            { $project: { _id: 0, } }
                        ],
                        nbCorporateNotif: [
                            { $match: { clientProfile: 100 } },
                            { $group: { _id: true, nb: { $sum: 1 } } },
                            { $project: { _id: 0, } }
                        ],
                        nbNetworkNotif: [
                            { $match: { clientProfile: 200 } },
                            { $group: { _id: true, nb: { $sum: 1 } } },
                            { $project: { _id: 0, } }
                        ]
                    }
                }
            ]
        );

        return query;
    },

    getStatisticsNotificationsStatus: () => {

        const query = [
            {

                $facet: {
                    nbUsersWthPhone: [
                        {
                            $match: {
                                $or:
                                    [
                                        { TELEPHONE_CLIENT: null },
                                        { TELEPHONE_CLIENT: '' },
                                        { TELEPHONE_CLIENT: { $exists: false } }
                                    ]
                            }
                        },
                        { $group: { _id: true, nb: { $sum: 1 } } },
                        { $project: { _id: 0, nb: 1 } }
                    ],
                    nbUsersWthEmail: [
                        {
                            $match: {
                                $or:
                                    [
                                        { EMAIL_CLIENT: null },
                                        { EMAIL_CLIENT: '' },
                                        { EMAIL_CLIENT: { $exists: false } }
                                    ]
                            }
                        },
                        { $group: { _id: true, nb: { $sum: 1 } } },
                        { $project: { _id: 0, nb: 1 } }
                    ],
                    nbUsersWthPhoneAndEmail: [
                        {
                            $match: {
                                $and:
                                    [
                                        {
                                            $or:
                                                [
                                                    { TELEPHONE_CLIENT: null },
                                                    { TELEPHONE_CLIENT: '' },
                                                    { TELEPHONE_CLIENT: { $exists: false } }
                                                ]
                                        },
                                        {
                                            $or:
                                                [
                                                    { EMAIL_CLIENT: null },
                                                    { EMAIL_CLIENT: '' },
                                                    { EMAIL_CLIENT: { $exists: false } }
                                                ]
                                        }
                                    ]
                            }
                        },
                        { $group: { _id: true, nb: { '$sum': 1 } } },
                        { $project: { _id: 0, nb: 1 } }
                    ],

                }

            }
        ];

        return query;
    },

    getStatisticsNotificationsStatus2: () => {

        const query = [
            {

                $facet: {
                    nbUsersWthPhone: [
                        {
                            $match: {
                                $or:
                                    [
                                        { TELEPHONE_CLIENT: { $exists: false } },
                                        { TELEPHONE_CLIENT: 'null' }
                                    ]
                            }
                        },
                        { $group: { _id: true, nb: { $sum: 1 } } },
                        { $project: { _id: 0, nb: 1 } }
                    ],
                    nbUsersWthEmail: [
                        {
                            $match: {
                                $or:
                                    [
                                        { EMAIL_CLIENT: { $exists: false } },
                                        { EMAIL_CLIENT: 'null' }
                                    ]
                            }
                        },
                        { $group: { _id: true, nb: { $sum: 1 } } },
                        { $project: { _id: 0, nb: 1 } }
                    ],
                    nbUsersWthPhoneAndEmail: [
                        {
                            $match: {
                                $and: [
                                    {
                                        $or:
                                            [
                                                { TELEPHONE_CLIENT: { $exists: false } },
                                                { TELEPHONE_CLIENT: 'null' }
                                            ]
                                    },
                                    {
                                        $or:
                                            [
                                                { EMAIL_CLIENT: { $exists: false } },
                                                { EMAIL_CLIENT: 'null' }
                                            ]
                                    }
                                ]
                            }
                        },
                        { $group: { _id: true, nb: { '$sum': 1 } } },
                        { $project: { _id: 0, nb: 1 } }
                    ],

                }

            }
        ];

        return query;
    },

    generateUnreachableUserQuery: () => {

        const query =
            [
                {
                    $match: {
                        $or:
                            [
                                { TELEPHONE_CLIENT: null },
                                { TELEPHONE_CLIENT: '' },
                                { EMAIL_CLIENT: null },
                                { EMAIL_CLIENT: '' }
                            ]
                    }
                },
                {
                    $group: {
                        _id: {
                            code: "$CODE_CLIENT",
                            sexe: "$SEXE_CLIENT",
                            nom_client: "$NOM_DU_CLIENT",
                            phone: "$TELEPHONE_CLIENT",
                            email: "$EMAIL_CLIENT",
                            profil: "$CODE_PROFIL_CLIENT"
                        },
                        nb: { $sum: 1 },
                        totalAllEch: { $sum: "$TOTAL_ECH" }
                    }
                },
                {
                    $project: {
                        _id: 0,
                        code: "$_id.code",
                        sexe: "$_id.sexe",
                        nom_client: "$_id.nom_client",
                        phone: "$_id.phone",
                        email: "$_id.email",
                        profil: "$_id.profil",
                        nb: 1,
                        totalAllEch: 1
                    }
                }
            ];

        return query;
    },

    generateUnreachableUserQuery2: () => {

        const query =
            [
                {
                    $match: {
                        $or:
                            [
                                { TELEPHONE_CLIENT: { $exists: false } },
                                { TELEPHONE_CLIENT: 'null' },
                                { EMAIL_CLIENT: { $exists: false } },
                                { EMAIL_CLIENT: 'null' }
                            ]
                    }
                },
                {
                    $group: {
                        _id: {
                            code: "$CODE_CLIENT",
                            sexe: "$SEXE_CLIENT",
                            nom_client: "$NOM_DU_CLIENT",
                            phone: "$TELEPHONE_CLIENT",
                            email: "$EMAIL_CLIENT",
                            profil: "$CODE_PROFIL_CLIENT"
                        },
                        nb: { $sum: 1 },
                        totalAllEch: { $sum: "$TOTAL_ECH" }
                    }
                },
                {
                    $project: {
                        _id: 0,
                        code: "$_id.code",
                        sexe: "$_id.sexe",
                        nom_client: "$_id.nom_client",
                        phone: "$_id.phone",
                        email: "$_id.email",
                        profil: "$_id.profil",
                        nb: 1,
                        totalAllEch: 1
                    }
                }
            ];

        return query;
    },

}
