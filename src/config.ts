import * as convict from 'convict';

// Define a schema
export const config = convict({
    env: {
        doc: 'The application environment.',
        format: ['production', 'development', 'staging', 'staging-bci'],
        default: 'development',
        env: 'NODE_ENV'
    },
    ip: {
        doc: 'The IP address to bind.',
        format: String,
        default: '127.0.0.1',
        env: 'IP_ADDRESS',
    },
    port: {
        doc: 'The port to bind.',
        format: 'port',
        default: 3000,
        env: 'PORT',
        arg: 'port'
    },
    host: {
        doc: 'Application host.',
        format: '*',
        default: 'localhost',
        env: 'HOST'
    },
    db: {
        host: {
            doc: 'Database host name/IP',
            format: '*',
            default: '127.0.0.1:27017',
            env: 'DB_MONGO_HOST'
        },
        name: {
            doc: 'Database name',
            format: String,
            default: '',
            env: 'DB_MONGO_NAME'
        },
        backupName: {
            doc: 'Backup database name',
            format: String,
            default: '',
            env: 'DB_MONGO_BACKUP_NAME'
        },
        auth: {
            user: {
                doc: 'Database user if any',
                format: String,
                default: '',
                env: 'DB_MONGO_USERNAME'
            },
            password: {
                doc: 'Database password if any',
                format: String,
                default: '',
                env: 'DB_MONGO_PASSWORD'
            }
        }
    },
    baseUrl: {
        doc: 'API base url.',
        format: String,
        default: '',
        env: 'BASE_URL',
        arg: 'base-url'
    },
    basePath: {
        doc: 'API base path.',
        format: String,
        default: ''
    },
    emailApiUrl: {
        doc: 'CBS API base url.',
        format: String,
        default: '',
        env: 'CBS_API_URL'
    },
    cbsApiUrl: {
        doc: 'CBS API base url.',
        format: String,
        default: '',
        env: 'CBS_API_URL'
    },
    operationsApiUrl: {
        doc: 'OPERATION API base url.',
        format: String,
        default: '',
        env: 'OPERATION_API_URL'
    },
    pdfApiUrl: {
        doc: 'PDF API base url.',
        format: String,
        default: '',
        env: 'PDF_API_URL'
    },
    notificationSettings: {
        activateLimitSMSNumber: {
            doc: 'ACTIVATE THE LIMITATION OF THE NUMBER OF SMS NOTIFICATIONS GENERATED',
            format: Boolean,
            default: true,
            env: 'ACTIVATE_LIMIT_SMS_NUMBER'
        },
        smsLimit: {
            doc: 'Limit SMS number.',
            format: Number,
            default: 100,
            env: 'LIMIT_SMS'
        },
        activateLimitEmailNumber: {
            doc: 'ACTIVATE THE LIMITATION OF THE NUMBER OF Email NOTIFICATIONS GENERATED',
            format: Boolean,
            default: true,
            env: 'ACTIVATE_LIMIT_EMAIL_NUMBER'
        },
        emailLimit: {
            doc: 'Limit Email number.',
            format: Number,
            default: 100,
            env: 'LIMIT_EMAIL'
        },
        limitPdf: {
            doc: 'LIMIT NUMBER PDF GENERATE.',
            format: Number,
            default: 100,
            env: 'LIMIT_PDF'
        },
        comexAddressReplacement: {
            doc: 'ADDRESS COMEX REPLACEMENT',
            format: String,
            default: '',
            env: 'COMEX_ADDRESS_REPLACEMENT'
        },
        activatecomexAddressReplacement: {
            doc: 'ACTIVATE ADDRESS COMEX REPLACEMENT',
            format: Boolean,
            default: false,
            env: 'ACTIVATE_COMEX_ADDRESS_REPLACEMENT'
        }
    },
    replacementEmailTable: {
        doc: 'TABLE THAT CONTAINS REPLACEMENT EMAILS',
        format: Array,
        default: [],
        env: 'REPLACEMENT_EMAIL'
    },
    replacementPhoneTable: {
        doc: 'TABLE THAT CONTAINS REPLACEMENT PHONES',
        format: Array,
        default: [],
        env: 'REPLACEMENT_PHONE'
    },
    activateEmailAndPhoneReplacement: {
        doc: 'ACTIVATE EMAIL AND PHONE REPLACEMENT',
        format: Boolean,
        default: false,
        env: 'ACTIVATE_EMAIL_PHONE_REPLACEMENT'
    },
    source: {
        client: {
            doc: 'Source client name.',
            format: String,
            default: 'BCI',
            env: 'SOURCE_CLIENT_NAME'
        },
        app: {
            doc: 'Source App name',
            format: String,
            default: '',
            env: 'SOURCE_APP_NAME'
        }
    }
});

// Load environment dependent configuration
const env = config.get('env');
config.loadFile('./src/config/' + env + '.json');

// Perform validation
config.validate({ allowed: 'strict' });