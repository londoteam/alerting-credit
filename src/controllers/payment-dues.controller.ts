import { commonService } from '../services/common.service';
import { paymentDuesService } from '../services/payment-dues.service';
import { Request, Response } from 'express';

export const paymentDuesController = {
    init: (app: any): void => {

        app.put('/notify/credits/payment-dues', async (req: Request, res: Response) => {
            await commonService.timeout(1000);
            const data = await paymentDuesService.updateReportingPaymentDues();
            if (data instanceof Error) {
                const message = 'internal server error.';
                const errResp = commonService.generateErrResponse(message, data);
                return res.status(500).json(errResp);
            }
            res.status(200).json({ message: 'payment dues updated successfully' });
        });
    }
}
