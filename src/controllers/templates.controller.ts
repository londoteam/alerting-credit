import { commonService } from '../services/common.service';
import { templatesService } from '../services/templates.service';
import { Request, Response } from 'express';

export const templatesController = {

    init: (app: any): void => {

        app.post('/notify/credits/templates', async (req: Request, res: Response) => {
            const data = await templatesService.insertTemplate(req.body);
            if (data instanceof Error) {
                const message = 'internal server error.';
                const errResp = commonService.generateErrResponse(message, data);
                return res.status(500).json(errResp);
            }
            res.status(200).json({ message: 'template inserted.' });
        });

        app.get('/notify/credits/templates', async (req: Request, res: Response) => {
            const data = await templatesService.getTemplates(req.query);
            if (data instanceof Error) {
                const message = 'internal server error.';
                const errResp = commonService.generateErrResponse(message, data);
                return res.status(500).json(errResp);
            }
            res.status(200).json(data);
        });

        app.put('/notify/credits/templates/:id', async (req: Request, res: Response) => {
            const { id } = req.params;
            const data = await templatesService.updateTemplateById(id, req.body);
            if (data instanceof Error) {
                const message = 'internal server error.';
                const errResp = commonService.generateErrResponse(message, data);
                return res.status(500).json(errResp);
            }
            res.status(200).json({ message: 'template updated.' });
        });
    }


}