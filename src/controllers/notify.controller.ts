import { reportingService } from '../services/reporting.service';
import { commonService } from '../services/common.service';
import { notifyService } from '../services/notify.service';
import { Request, Response } from 'express';

export const notifyController = {

    init: (app: any): void => {

        app.get('/notify/credits/reporting/cae', async (req: Request, res: Response) => {
            await commonService.timeout(1000);
            const data = await reportingService.getCaeReporting();
            if (data instanceof Error) {
                const message = 'internal server error.';
                const errResp = commonService.generateErrResponse(message, data);
                return res.status(500).json(errResp);
            }
            res.status(200).json({ message: 'Reporting email successfully sent to all cae.' });
        });

        app.get('/notify/credits/reporting/age-manager', async (req: Request, res: Response) => {
            await commonService.timeout(1000);
            const data = await reportingService.getAgeManagerReporting();
            if (data instanceof Error) {
                const message = 'internal server error.';
                const errResp = commonService.generateErrResponse(message, data);
                return res.status(500).json(errResp);
            }
            res.status(200).json({ message: 'Reporting email successfully sent to all agency manager.' });
        });

        app.get('/notify/credits/reporting/rg-manager', async (req: Request, res: Response) => {
            await commonService.timeout(1000);
            const data = await reportingService.getRgManagerReporting();
            if (data instanceof Error) {
                const message = 'internal server error.';
                const errResp = commonService.generateErrResponse(message, data);
                return res.status(500).json(errResp);
            }
            res.status(200).json({ message: 'Reporting email successfully sent to all regional manager.' });
        });

        app.get('/notify/credits/reporting/top-manager', async (req: Request, res: Response) => {
            await commonService.timeout(1000);
            const data = await reportingService.getTopManagerReporting();
            if (data instanceof Error) {
                const message = 'internal server error.';
                const errResp = commonService.generateErrResponse(message, data);
                return res.status(500).json(errResp);
            }
            res.status(200).json({ message: 'Reporting email successfully sent to top management.' });
        });

        app.post('/notify/credits/generate/credit-alerts', async (req: Request, res: Response) => {
            await commonService.timeout(1000);
            res.status(200).json({ message: `processing request.` });
            await notifyService.generateNotifications();
        });

        app.post('/notify/credits/generate/bank-reporting', async (req: Request, res: Response) => {
            await commonService.timeout(1000);
            const data = await reportingService.startBankReportingProcess();
            if (data instanceof Error) {
                const message = 'internal server error.';
                const errResp = commonService.generateErrResponse(message, data);
                return res.status(500).json(errResp);
            }
            res.status(200).json({ message: 'Reporting email successfully sent to bank staff.' });
        });

    }

}