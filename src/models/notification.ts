import { OperationCode } from "../enums/operations.enum";


export interface Notification {
    message?: string;
    clientCode?: string;
    subject?: string;
    subjectFR?: string;
    subjectEN?: string;
    age?: string;
    status?: number; // 100 (Created), 200 (Success Sent), 300 (Unsuccess Sent)
    type?: number;
    format?: number;
    smsCount?: number;
    priority?: number;
    clientProfile?: number;
    operationCode?: OperationCode;
    telephone?: string;
    dates?: { createdAt?: number, sendAt?: number };
    email?: string;
    source?: Source;
    first_reminder_date?: string;
}

export interface Source {
    client: string;
    app: any;
}