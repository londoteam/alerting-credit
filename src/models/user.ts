
export interface User {
    _id?: string;
    userCode?: number;
    fname?: string;
    lname?: string;
    tel?: string;
    email?: string;
    category?: string;
}
