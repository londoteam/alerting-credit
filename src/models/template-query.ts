export interface QueryVariable {
    name?: string;
    desc?: string;
    type?: 'STRING' | 'DATE' | 'NUMBER';
}

export interface TemplateQuery {
    _id?: string;
    ref?: string;
    label?: string;
    desc?: string;
    sqlCode?: string;
    variables?: QueryVariable[];
    dates?: {
        created?: number;
        updated?: number;
    };
}