export interface TemplateForm {
    _id?: string;
    label?: string;
    desc?: string;
    subjectFR?: string;
    subjectEN?: string;
    templateType?: number; // 1: avant échéance; 2: impayé; 3: première relance; 4: 2éme relance
    clientProfile?: number; // 100: particuliers; 200: Résaux;
    operationCode?: string;
    notifyDate?: string;
    sms?: Message;
    email?: Message;
    author?: any;
    enabled?: boolean;
    variables?: Variable[];
    dates?: { created?: number; updated?: number; };
    language?: string;
    priority?: number; // 1: envoi instantané; 2: envoi journalier (heure à définir); 3: envoi hebdomadaire (jour et heure à définir);
}

export interface Message {
    frenchText?: string;
    englishText?: string;
}

export interface Variable {
    name?: string;
    desc?: string;
    type?: 'STRING' | 'DATE' | 'NUMBER';
}