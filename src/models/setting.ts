import { TemplateQuery } from './template-query';

export interface Setting {
    _id?: string;
    cost?: number;
    type?: number; // 1: sms; 2: email
    nbSimultaneousSending?: number;
    quota?: number;
    bridge?: any;
}
