import { templatesCollection } from '../collections/templates.collection';
import { commonService } from '../services/common.service';
import { TemplateQuery } from '../models/template-query';
import * as httpContext from 'express-http-context';
import { TemplateForm } from '../models/template';
import { User } from '../models/user';
import { logger } from '../winston';
import * as moment from 'moment';


export const templatesService = {

    insertTemplate: async (template: TemplateForm): Promise<any> => {
        const user: User = httpContext.get('user');
        try {
            // Add template author
            template.author = `${user.fname} ${user.lname}`;
            // Add template date create
            template.dates = {};
            template.dates.created = moment().valueOf();
            return await templatesCollection.insertTemplate(template);
        } catch (error) {
            logger.error(`failed to insert template \n${error.name} \n${error.stack}`);
            return error;
        }
    },

    getTemplates: async (filters: any): Promise<TemplateQuery[]> => {
        try {
            commonService.parseNumberFields(filters);
            return await templatesCollection.getTemplates(filters);
        } catch (error) {
            logger.error(`failed to get templates \n${error.name} \n${error.stack}`);
            return error;
        }
    },

    getTemplateBy: async (filters: any): Promise<TemplateQuery> => {
        try {
            return await templatesCollection.getTemplateBy(filters);
        } catch (error) {
            logger.error(`failed to get template by filter \n${error.name} \n${error.stack}`);
            return error;
        }
    },

    updateTemplateById: async (id: string, template: TemplateForm) => {
        try {
            await templatesCollection.updateTemplateById(id, template);
        } catch (error) {
            logger.error(`failed to update template by id \n${error.name} \n${error.stack}`);
            return error;
        }
    }

};