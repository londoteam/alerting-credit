import { notifyService } from "./notify.service";
import { logger } from "../winston";
import * as cron from 'node-cron';

export const cronService = {

    checkAndSendCreditsNotifications: async (): Promise<void> => {
        const cronExpression = '* 7-21 * * *'; // At every minute past every hour from 7 through 21
        cron.schedule(cronExpression, async () => {
            try {
                await notifyService.checkAndSendCreditsNotifications();
            } catch (error) {
                logger.error(`check and send campaign failed \n${error.stack}\n`);
                process.exit(1);
            }
        });

    },

};



