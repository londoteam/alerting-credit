import { logger } from '../winston';
import { tfjDatesCollection } from '../collections/tfj-dates.collection';
import { commonService } from './common.service';
import { UpdateWriteOpResult } from 'mongodb';


export const tfjDatesService = {

    insertTfjDate: async (tfjDate: any): Promise<any> => {
        try {
            return await tfjDatesCollection.insertTfjDate(tfjDate);
        } catch (error) {
            logger.error(`insert tfj date failed \n${error.name} \n${error.stack}`);
            return error;
        }
    },

    getLastTfjDate: async (): Promise<any> => {
        try {
            return await tfjDatesCollection.getLastTfjDate();
        } catch (error) {
            logger.error(`fetch tfj date list failed \n${error.name} \n${error.stack}`);
            return error;
        }
    },

    getTfjDateBy: async (filters: any): Promise<any[]> => {
        commonService.parseNumberFields(filters);
        try {
            return await tfjDatesCollection.getTfjDateBy(filters);
        } catch (error) {
            logger.error(`fetch tfj date by filter failed  \n${error.name} \n${error.stack}`);
            return error;
        }
    },

    updateLastTfjDate: async (tfjDate: any): Promise<UpdateWriteOpResult> => {
        try {
            return await tfjDatesCollection.updateLastTfjDate(tfjDate);
        } catch (error) {
            logger.error(`update tfj data failed \n${error.name} \n${error.stack}`);
            return error;
        }
    },

};