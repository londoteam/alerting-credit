import { logger } from '../winston';
import { unreachablesClientCollection } from '../collections/unreachable-client.collection';
import { commonService } from './common.service';
import { UpdateWriteOpResult } from 'mongodb';


export const unreachablesClientService = {

    getUnreachableClients: async () => {
        try {
            const unreachables = await unreachablesClientCollection.getAggrateUnreachableClients();
            return unreachables;
        } catch (error) {
            logger.error(`failed to fetch unreachable clients data \n${error.name} \n${error.stack}`);
            return error;
        }

    },

    getUnreachables: async (filters: any): Promise<any[]> => {
        try {
            commonService.parseNumberFields(filters);
            const { offset, limit } = filters;
            logger.info(`fetching notications filters: ${JSON.stringify(filters)}`);
            delete filters.offset;
            delete filters.limit;
            return await unreachablesClientCollection.getUnreachables(filters || {}, offset || 1, limit || 40);
        } catch (error) {
            logger.error(`fetch unreachables list failed \n${error.stack}`);
            return error;
        }
    },

    getUnreachableBy: async (filters: any): Promise<any[]> => {
        commonService.parseNumberFields(filters);
        try {
            return await unreachablesClientCollection.getUnreachableBy(filters);
        } catch (error) {
            logger.error(`fetch unreachable by filter failed \n${error.stack}`);
            return error;
        }
    },

    updateUnreachableById: async (id: string, unreachable: any): Promise<UpdateWriteOpResult> => {
        try {
            return await unreachablesClientCollection.updateUnreachableById(id, unreachable);
        } catch (error) {
            logger.error(`update unreachable failed \n${error.stack}`);
            return error;
        }
    },

};