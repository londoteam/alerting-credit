import { reportingsCollection } from '../../collections/reporting.collection';
import { OperationCode } from '../../enums/operations.enum';
import { logger } from '../../winston';
import * as moment from 'moment';
import { get } from 'lodash';
import { config } from '../../config';
moment.locale('fr');


export const helper = {

    generateEmailNotication: (to: any, subject: string, body: any, files?: any, cc?: any) => {
        const email: any = {
            subject,
            email: to,
            status: 100,
            format: 200,
            message: body,
            priority: 3,
            dates: { createdAt: moment().valueOf() },
            operationCode: OperationCode.CREDIT,
        }
        return email;
    },

    generaCaeTemplateData: (data: any[]) => {
        const LISTUSERSUNPAIDECH: any[] = get(data[0], 'clientListUnpaidEch', []);
        const UNPAIDPAYMENTDUES: any[] = get(data[0], 'totalUnpaidEch', []);
        const PAIDPAYMENTDUES: any[] = get(data[0], 'totalpaidEch', []);
        const DUEPAYMENTDUES: any[] = get(data[0], 'totalDueEch', []);
        const TOTALPAYMENTDUES: any[] = get(data[0], 'totalEch', []);

        if (TOTALPAYMENTDUES.length === 0) { return; }

        return TOTALPAYMENTDUES.map((paymentDue: any) => {

            // Set paid payment dues data
            const paidPaymentDue = PAIDPAYMENTDUES.find((elmt: any) => (elmt.code === paymentDue.code) && (elmt.age === paymentDue.age));
            paymentDue.nbpaid = get(paidPaymentDue, 'nbEch', 0);
            paymentDue.totalpaid = get(paidPaymentDue, 'totalEch', 0);

            // Set due payment dues data
            const duePaymentDue = DUEPAYMENTDUES.find((elmt: any) => (elmt.code === paymentDue.code) && (elmt.age === paymentDue.age));
            paymentDue.nbDue = get(duePaymentDue, 'nbEch', 0);
            paymentDue.totalDue = get(duePaymentDue, 'totalEch', 0);

            // Set unpaid payment dues data
            const unpaidPaymentDue = UNPAIDPAYMENTDUES.find((elmt: any) => (elmt.code === paymentDue.code) && (elmt.age === paymentDue.age));
            // paymentDue.nbUnpaid = get(unpaidPaymentDue, 'nbEch', 0);
            // paymentDue.totalUnpaid = get(unpaidPaymentDue, 'totalEch', 0);
            paymentDue.nbUnpaid = get(paymentDue, 'nbEch', 0) - paymentDue.nbpaid;
            paymentDue.totalUnpaid = get(paymentDue, 'totalEch', 0) - paymentDue.totalpaid;

            // Set unpaid client list
            const unpaidClientList = LISTUSERSUNPAIDECH.filter((elmt: any) => (elmt.code === paymentDue.code) && (elmt.age === paymentDue.age));
            paymentDue.unpaidClientList = unpaidClientList || [];
            return paymentDue
        });
    },

    generateMailContent: (data: any, start: any, end: any) => {

        try {
            const nbEch = get(data, 'nbEch', 0);
            const paidEch = get(data, 'totalpaid', 0);
            const totalEch = get(data, 'totalEch', 0);
            const nbPaidEch = get(data, 'nbpaid', 0);
            // get unpaid rate
            const completionRate = nbPaidEch / nbEch;
            const unpaidRate = nbEch > 0 ? ((1 - completionRate) * 100).toFixed(2) : 0;

            // Add unpaid client list
            const unpaidClients = [];
            const clientList = get(data, 'unpaidClientList', []);
            clientList.forEach((client: any, index: number) => {
                unpaidClients.push({
                    client_number: `${index + 1}`,
                    client_tel: `${get(client, 'clientTel')}`,
                    client_name: `${get(client, 'clientName')}`,
                    client_dateEch: `${moment(parseInt(get(client, 'dateEch'), 10)).format('DD/MM/YYYY')}`,
                    client_amountEch: `${getNumberWithSpaces(get(client, 'totalEch'))}`,
                });
            });

            const templateData = {
                nbEch: `${getNumberWithSpaces(nbEch)}`,
                paidEch: `${getNumberWithSpaces(paidEch)}`,
                totalEch: `${getNumberWithSpaces(totalEch)}`,
                nbPaidEch: `${getNumberWithSpaces(nbPaidEch)}`,
                date: `${moment().format('DD MMMM YYYY')}`,
                caeFullName: `${get(data, 'fullName', '')},`,
                email: `${get(data, 'email', '')},`,
                unpaidClients: unpaidClients || [],
                start_date: `${start}`,
                end_date: `${end}`,
                unpaidRate,
            }

            return templateData;

        } catch (error) {
            logger.error(`failed to generate cae template mail content \n${error.stack}`);
            return error;
        }

    },

    generaAgeManagerTemplateData: (data: any[], ages: any[]) => {
        // tslint:disable-next-line: one-variable-per-declaration
        let nbCae = 0, nbEch = 0, totalEch = 0, nbPaidEch = 0, paidEch = 0, nbDueEch = 0, dueEch = 0, nbUnpaidEch = 0, unpaidEch = 0;
        let caeList = [];
        const agencyPayment = [];

        for (const age of ages) {
            nbCae = nbEch = totalEch = nbPaidEch = paidEch = nbUnpaidEch = unpaidEch = nbDueEch = dueEch = 0;
            caeList = [];
            data.forEach((reportingCaeElmt) => {
                const agency = get(reportingCaeElmt, 'age', '');
                if (age.AGE === agency) {
                    nbCae += 1;
                    nbEch += parseInt(get(reportingCaeElmt, 'nbEch', 0), 10);
                    totalEch += parseInt(get(reportingCaeElmt, 'totalEch', 0), 10);
                    nbDueEch += parseInt(get(reportingCaeElmt, 'nbDue', 0), 10);
                    dueEch += parseInt(get(reportingCaeElmt, 'totalDue', 0), 10);
                    nbPaidEch += parseInt(get(reportingCaeElmt, 'nbpaid', 0), 10);
                    paidEch += parseInt(get(reportingCaeElmt, 'totalpaid', 0), 10);
                    nbUnpaidEch += parseInt(get(reportingCaeElmt, 'nbUnpaid', 0), 10);
                    unpaidEch += parseInt(get(reportingCaeElmt, 'totalUnpaid', 0), 10);
                    const caeUnpaidRate = nbEch > 0 ? ((nbUnpaidEch / nbEch) * 100).toFixed(2) : 0;
                    const caeDueRate = nbEch > 0 ? ((nbDueEch / nbEch) * 100).toFixed(2) : 0;
                    caeList.push({ fullName: get(reportingCaeElmt, 'fullName', ''), nbEch: get(reportingCaeElmt, 'nbEch', 0), caeUnpaidRate, caeDueRate, paidEch: get(reportingCaeElmt, 'totalpaid', 0), code: get(reportingCaeElmt, 'code', 0) });
                }
            });
            // get unpaid rate
            const unpaidRate = nbEch > 0 ? ((nbUnpaidEch / nbEch) * 100).toFixed(2) : 0;

            const agencyPaymentElmt = { nbCae, nbEch, totalEch, nbPaidEch, paidEch, nbDueEch, dueEch, nbUnpaidEch, unpaidEch, unpaidRate, caeList, age: { code: age.AGE || '', libage: age.LIBAGE || '', email_da: age.EMAIL_DA || '', email_dr: age.EMAIL_DR || '', email_dr_adj_1: age.EMAIL_DR_ADJ_1 || '', email_dr_adj_2: age?.EMAIL_DR_ADJ_2 || '', codeReg: age.CODEREG || '', libReg: age.LIBREG || '' } };
            agencyPayment.push(agencyPaymentElmt);
        };
        return agencyPayment;
    },

    generateAgeManagerMailContent: (data: any, start: any, end: any) => {

        try {
            const templateData = {
                nbCae: `${getNumberWithSpaces(get(data, 'nbCae', 0))}`,
                nbEch: `${getNumberWithSpaces(get(data, 'nbEch', 0))}`,
                paidEch: `${getNumberWithSpaces(get(data, 'paidEch', 0))}`,
                totalEch: `${getNumberWithSpaces(get(data, 'totalEch', 0))}`,
                nbPaidEch: `${getNumberWithSpaces(get(data, 'nbPaidEch', 0))}`,
                unpaidEch: `${getNumberWithSpaces(get(data, 'unpaidEch', 0))}`,
                nbUnpaidEch: `${getNumberWithSpaces(get(data, 'nbUnpaidEch', 0))}`,
                caeFullName: `${get(data, 'fullName', '')},`,
                unpaidRate: `${get(data, 'unpaidRate', 0)}`,
                date: `${moment().format('DD MMMM YYYY')}`,
                email: `${get(data, 'age.email_da', '')},`,
                libage: `${get(data, 'age', '').libage},`,
                start_date: `${start}`,
                end_date: `${end}`,
                caeList: get(data, 'caeList', []).map((elmt) => {
                    elmt.paidEch = getNumberWithSpaces(elmt.paidEch);
                    return elmt;
                }),
            }

            return templateData;

        } catch (error) {
            logger.error(`failed to generate age manager template mail content \n${error.stack}`);
            return error;
        }
    },

    generaRgManagerTemplateData: async (data: any[], regions: any[]) => {
        // tslint:disable-next-line: one-variable-per-declaration
        let nbCae = 0, nbEch = 0, totalEch = 0, nbPaidEch = 0, paidEch = 0, nbDueEch = 0, dueEch = 0, nbUnpaidEch = 0, unpaidEch = 0, nbAge = 0;
        let ageList = [];
        const regionPayment = [];

        for (const region of regions) {
            nbAge = nbCae = nbEch = totalEch = nbPaidEch = paidEch = nbDueEch = dueEch = nbUnpaidEch = unpaidEch = 0;
            ageList = [];
            data.map((reportingAgeElmt) => {
                if (region.code === reportingAgeElmt.age.codeReg) {
                    nbAge += 1;
                    nbEch += reportingAgeElmt.nbEch;
                    totalEch += reportingAgeElmt.totalEch;
                    nbPaidEch += reportingAgeElmt.nbPaidEch;
                    paidEch += reportingAgeElmt.paidEch;
                    nbDueEch += reportingAgeElmt.nbDueEch;
                    dueEch += reportingAgeElmt.dueEch;
                    nbUnpaidEch += reportingAgeElmt.nbUnpaidEch;
                    unpaidEch += reportingAgeElmt.unpaidEch;
                    ageList.push(reportingAgeElmt);
                }
            });
            // get paid rate
            const unpaidRate = nbEch > 0 ? ((nbUnpaidEch / nbEch) * 100).toFixed(2) : 0;
            const paidRate = nbEch > 0 ? ((nbPaidEch / nbEch) * 100).toFixed(2) : 0;
            const dueRate = nbEch > 0 ? ((nbDueEch / nbEch) * 100).toFixed(2) : 0;

            const caeRegionData = await reportingsCollection.getCaesRegionList();
            const CAESLIST: any[] = get(caeRegionData[0], 'totalCae', []);
            const RCCAELIST: any[] = get(caeRegionData[0], 'totalRCCae', []);
            const RMCAELIST: any[] = get(caeRegionData[0], 'totalRMCae', []);

            // add Cae number
            nbCae = (region.code === 'DRM') ? RMCAELIST.length : RCCAELIST.length;

            const regionPaymentElmt = { libreg: region.label, codereg: region.code, email_dr: region.email_dr, email_dr_adj_1: region.email_dr_adj_1, email_dr_adj_2: region.email_dr_adj_2, nbAge, nbCae, nbEch, totalEch, nbPaidEch, paidEch, nbDueEch, dueEch, nbUnpaidEch, unpaidEch, unpaidRate, paidRate, dueRate, ageList };
            regionPayment.push(regionPaymentElmt);
        };
        return regionPayment;
    },

    generateRgManagerMailContent: (data: any, start: any, end: any) => {

        try {
            const ageList = [];
            get(data, 'ageList', []).forEach((elmt: any) => {
                elmt.paidEch = getNumberWithSpaces(elmt.paidEch);
                elmt.totalEch = getNumberWithSpaces(elmt.totalEch);
                elmt.unpaidEch = getNumberWithSpaces(elmt.unpaidEch);
                const age = {
                    nbEch: elmt.nbEch,
                    code: elmt.age.code,
                    paidEch: elmt.paidEch,
                    libage: elmt.age.libage,
                    totalEch: elmt.totalEch,
                    unpaidEch: elmt.unpaidEch,
                    unpaidRate: elmt.unpaidRate,
                };
                ageList.push(age);
            });
            const templateData = {
                nbAge: `${getNumberWithSpaces(get(data, 'nbAge', 0))}`,
                nbCae: `${getNumberWithSpaces(get(data, 'nbCae', 0))}`,
                nbEch: `${getNumberWithSpaces(get(data, 'nbEch', 0))}`,
                paidEch: `${getNumberWithSpaces(get(data, 'paidEch', 0))}`,
                totalEch: `${getNumberWithSpaces(get(data, 'totalEch', 0))}`,
                nbPaidEch: `${getNumberWithSpaces(get(data, 'nbPaidEch', 0))}`,
                unpaidEch: `${getNumberWithSpaces(get(data, 'unpaidEch', 0))}`,
                nbUnpaidEch: `${getNumberWithSpaces(get(data, 'nbUnpaidEch', 0))}`,
                unpaidRate: `${get(data, 'unpaidRate', 0)}`,
                date: `${moment().format('DD MMMM YYYY')}`,
                libreg: `${get(data, 'libreg', '')}`,
                start_date: `${start}`,
                end_date: `${end}`,
                ageList,
            }

            return templateData;

        } catch (error) {
            logger.error(`failed to generate region manager template mail content \n${error.stack}`);
            return error;
        }
    },

    generaTopManagerTemplateData: (data: any[]) => {
        // tslint:disable-next-line: one-variable-per-declaration
        let nbCae = 0, nbEch = 0, totalEch = 0, nbPaidEch = 0, paidEch = 0, nbDueEch = 0, dueEch = 0, nbUnpaidEch = 0, unpaidEch = 0, nbAge = 0;
        const regionPayment = [];
        data.forEach((reportingRgElmt: any) => {
            nbAge += reportingRgElmt.nbAge;
            nbCae += reportingRgElmt.nbCae;
            nbEch += reportingRgElmt.nbEch;
            totalEch += reportingRgElmt.totalEch;
            nbPaidEch += reportingRgElmt.nbPaidEch;
            paidEch += reportingRgElmt.paidEch;
            nbDueEch += reportingRgElmt.nbDueEch;
            dueEch += reportingRgElmt.dueEch;
            nbUnpaidEch += reportingRgElmt.nbUnpaidEch;
            unpaidEch += reportingRgElmt.unpaidEch;
        });

        // get paid and unpaid rate
        const unpaidRate = nbEch > 0 ? ((nbUnpaidEch / nbEch) * 100).toFixed(2) : 0;
        const paidRate = nbEch > 0 ? ((nbPaidEch / nbEch) * 100).toFixed(2) : 0;
        const dueRate = nbEch > 0 ? ((nbDueEch / nbEch) * 100).toFixed(2) : 0;

        const regionPaymentElmt = { libreg: 'BICEC', nbAge, nbCae, nbEch, totalEch, nbPaidEch, paidEch, nbDueEch, dueEch, nbUnpaidEch, unpaidEch, unpaidRate, paidRate, dueRate };
        regionPayment.push(regionPaymentElmt);
        data.forEach((reportingRgElmt: any) => regionPayment.push(reportingRgElmt));

        return regionPayment;
    },

    generateTopManagerMailContent: (data: any, start: any, end: any) => {

        try {
            const ageList = [];

            const recapData = [];
            data.forEach((reportingRgElmt: any) => {

                const recap = {
                    nbAge: `${getNumberWithSpaces(get(reportingRgElmt, 'nbAge', 0))}`,
                    nbCae: `${getNumberWithSpaces(get(reportingRgElmt, 'nbCae', 0))}`,
                    nbEch: `${getNumberWithSpaces(get(reportingRgElmt, 'nbEch', 0))}`,
                    paidEch: `${getNumberWithSpaces(get(reportingRgElmt, 'paidEch', 0))}`,
                    totalEch: `${getNumberWithSpaces(get(reportingRgElmt, 'totalEch', 0))}`,
                    nbPaidEch: `${getNumberWithSpaces(get(reportingRgElmt, 'nbPaidEch', 0))}`,
                    dueEch: `${getNumberWithSpaces(get(reportingRgElmt, 'dueEch', 0))}`,
                    nbDueEch: `${getNumberWithSpaces(get(reportingRgElmt, 'nbDueEch', 0))}`,
                    unpaidEch: `${getNumberWithSpaces(get(reportingRgElmt, 'unpaidEch', 0))}`,
                    nbUnpaidEch: `${getNumberWithSpaces(get(reportingRgElmt, 'nbUnpaidEch', 0))}`,
                    unpaidRate: `${get(reportingRgElmt, 'unpaidRate', 0)}`,
                    paidRate: `${get(reportingRgElmt, 'paidRate', 0)}`,
                    dueRate: `${get(reportingRgElmt, 'dueRate', 0)}`,
                    libReg: (reportingRgElmt.codereg) ? `Région ${get(reportingRgElmt, 'libreg', '')}` : `${get(reportingRgElmt, 'libreg', '')}`,
                };
                recapData.push(recap);
                get(reportingRgElmt, 'ageList', []).forEach((reportingAgeElmt: any) => {
                    reportingAgeElmt.paidEch = getNumberWithSpaces(reportingAgeElmt.paidEch);
                    const age = {
                        nbEch: reportingAgeElmt.nbEch,
                        code: reportingAgeElmt.age.code,
                        paidEch: reportingAgeElmt.paidEch,
                        unpaidEch: reportingAgeElmt.unpaidEch,
                        totalEch: reportingAgeElmt.totalEch,
                        libage: reportingAgeElmt.age.libage,
                        libReg: reportingAgeElmt.age.libReg,
                        unpaidRate: reportingAgeElmt.unpaidRate,
                    };
                    ageList.push(age);
                });
            });

            const templateData = {
                date: `${moment().format('DD MMMM YYYY')}`,
                start_date: `${start}`,
                end_date: `${end}`,
                recapData,
                ageList,
            }

            return templateData;

        } catch (error) {
            logger.error(`failed to generate top management template mail content \n${error.stack}`);
            return error;
        }
    },

    generateNotifications: (data: any[], subject: any, template: any, start: any, end: any) => {

        if (get(data, 'length') === 0) { return; }

        let notifications: any[] = data.map((reportingElmt: any) => {

            const mailContent = helper.generateMailContent(reportingElmt, start, end);
            const htmlBody = template(mailContent);

            const splitted = subject.split(/\n/);
            const message = splitted.filter((elt: any) => !!elt);

            const notificationElmt: any = {
                htmlBody,
                status: 100,
                subject,
                message,
                priority: 3,
                operationCode: OperationCode.CREDIT,
                format: 200,
                dates: { createdAt: moment().valueOf() },
                email: get(reportingElmt, 'email', ''),
                source: config.get('source')
            }

            return notificationElmt
        });

        notifications = notifications.filter(notification => notification.email);

        if (config.get('notificationSettings.activateLimitEmailNumber') === true) {
            notifications = notifications.slice(0, config.get('notificationSettings.emailLimit'));
        }

        return notifications;
    },

    generateAgeNotifications: (data: any[], subject: any, template: any, start: any, end: any) => {

        if (get(data, 'length') === 0) { return; }

        let notifications: any[] = data.map((reportingElmt: any) => {

            const mailContent = helper.generateMailContent(reportingElmt, start, end);
            const htmlBody = template(mailContent);
            let cc = '';

            if (config.get('notificationSettings.activatecomexAddressReplacement'))  {
                cc = config.get('notificationSettings.comexAddressReplacement');
            }

            const splitted = subject.split(/\n/);
            const message = splitted.filter((elt: any) => !!elt);

            const notificationElmt: any = {
                htmlBody,
                status: 100,
                subject,
                message,
                cc,
                priority: 3,
                operationCode: OperationCode.CREDIT,
                format: 200,
                dates: { createdAt: moment().valueOf() },
                email: get(reportingElmt, 'age.email_da', ''),
                source: config.get('source')
            }

            return notificationElmt
        });

        notifications = notifications.filter(notification => notification.email);

        if (config.get('notificationSettings.activateLimitEmailNumber') === true) {
            notifications = notifications.slice(0, config.get('notificationSettings.emailLimit'));
        }

        return notifications;
    },

    generateRegionNotifications: (subject: any, htmlBody: any, email_dr: any, email_adj: any) => {

        if (!htmlBody || !email_dr) { return; }

        const splitted = subject.split(/\n/);
        const message = splitted.filter((elt: any) => !!elt);

        const notification: any = {
            htmlBody,
            status: 100,
            subject,
            message,
            priority: 3,
            operationCode: OperationCode.CREDIT,
            format: 200,
            dates: { createdAt: moment().valueOf() },
            email: `${email_dr}`,
            cc: `${email_adj}`,
            source: config.get('source')
        }

        return notification;
    },

    generateTopManagerNotifications: (emailSiege: any, subject: any, htmlBody: any) => {

        if (!htmlBody || !emailSiege || !subject) { return; }

        const splitted = subject.split(/\n/);
        const message = splitted.filter((elt: any) => !!elt);

        const notification: any = {
            htmlBody,
            status: 100,
            subject,
            message,
            priority: 3,
            operationCode: OperationCode.CREDIT,
            format: 200,
            dates: { createdAt: moment().valueOf() },
            email: `${emailSiege}`,
            source: config.get('source')
        }

        return notification;
    },

}


const getNumberWithSpaces = (x: any) => {
    if (!x) { return '0' }
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}
