import { successfulCreditNotifysCollection } from '../../collections/successful-credit-notify.collection';
import { ClientProfile } from '../../enums/client-profile.enum';
import { OperationCode } from '../../enums/operations.enum';
import { Notification } from '../../models/notification';
import { commonService } from '../common.service';
import momentT = require('moment-timezone');
import { logger } from '../../winston';
import { config } from '../../config';
import { compile } from 'handlebars';
import { readFileSync } from 'fs';
import * as moment from 'moment';
import { get } from 'lodash';

const mailTemplate = readFileSync(__dirname + '/templates/notify-mail.template.html');
moment.locale('fr');
const contactsSMS = config.get('replacementPhoneTable');
const contactsEmail = config.get('replacementEmailTable');

export const helper = {

    getTemplateDateFilter: (template: any, dayFilter?: number) => {

        if (!get(template, 'notifyDate')) { return; }

        logger.info(`Date filter Notify date ${get(template, 'notifyDate')}; template : ${get(template, 'label')}`);
        // Get notify date operator to use (' - ' or ' + ')
        const notifyDateOperator = template.notifyDate.includes('-') ? '-' : '+';

        // Get notify date days number to use ( 7 || 14 || 30 ... )
        const numberOfDays = template.notifyDate.split(`${notifyDateOperator}`)[1];

        // Set due date filter for payment dues GET query
        const dueDate = notifyDateOperator === '-'
            ? moment(dayFilter).add(numberOfDays, "days")
            : moment(dayFilter).subtract(numberOfDays, "days");

        let start: any = moment(dueDate).startOf('day');
        start = moment(start).valueOf();
        let end: any = moment(dueDate).endOf('day');
        end = moment(end).valueOf();
        const dateFilterRange = { start, end };

        logger.info(`Date range filter Notify date ${JSON.stringify(dateFilterRange)}`);

        return dateFilterRange;
    },

    generateSMS: async (data: any[], template: any) => {

        if (!get(template, 'sms.frenchText') || get(data, 'length') === 0) { return; }

        let notifications: any[] = [];
        for (const paymentDue of data) {
            let firstReminderDate: any;
            if (template.templateType === 4) {
                // Check if fist letter reminder was sent before sending second reminder
                firstReminderDate = await getFirstReminderDate(paymentDue);
                if (firstReminderDate) {
                    paymentDue.first_reminder_date = `${moment(firstReminderDate).format('DD/MM/YY') || 'N/A'}`;
                }
            }

            const message = getInterpolatedMsg(template, paymentDue, 'sms');
            const smsCount = Math.ceil(message.length / 160);
            if (get(paymentDue, 'TELEPHONE_CLIENT')) {
                paymentDue.TELEPHONE_CLIENT = get(paymentDue, 'TELEPHONE_CLIENT', '').replace('+', '');
            }

            const notificationElmt: Notification = {
                message,
                smsCount,
                status: 100,
                format: 100,
                priority: get(template, 'priority'),
                type: get(template, 'templateType'),
                age: get(paymentDue, 'AGE_CPT', ''),
                operationCode: OperationCode.CREDIT,
                dates: { createdAt: moment().valueOf() },
                clientCode: get(paymentDue, 'CODE_CLIENT', ''),
                clientProfile: get(template, 'clientProfile', ''),
                telephone: get(paymentDue, 'TELEPHONE_CLIENT', ''),
                source: { client: 'BCI', app: OperationCode.CREDIT },
            }

            if (template.templateType === 4 && firstReminderDate) {
                notificationElmt.first_reminder_date = firstReminderDate;
            }

            notifications.push(notificationElmt);
        }

        notifications = notifications.filter(notification => {
            return notification.telephone && checkIsCongonianNumber(notification.telephone);
        })

        if (config.get('notificationSettings.activateLimitSMSNumber') === true) {
            notifications = notifications.slice(0, config.get('notificationSettings.smsLimit'));
        }
        if (config.get('activateEmailAndPhoneReplacement')) {
            notifications.map((notification: Notification) => {
                notification.telephone = contactsSMS[commonService.getRandomInt(0, (contactsSMS.length - 1))];
            });
        }

        return notifications;
    },

    generateEmails: async (paymentDues: any[], template: any, hasAttachment?: any) => {

        if (!get(template, 'email.frenchText') || get(paymentDues, 'length') === 0) { return; }

        let notifications: any[] = [];
        for (const paymentDue of paymentDues) {
            let firstReminderDate: any;
            if (template.templateType === 4) {
                // Check if fist letter reminder was sent before sending second reminder
                firstReminderDate = await getFirstReminderDate(paymentDue);
                if (firstReminderDate) {
                    paymentDue.first_reminder_date = `${moment(firstReminderDate).format('L') || 'N/A'}`;
                }
            }

            paymentDue.TOTAL_ECH = `${getNumberWithSpaces(get(paymentDue, 'TOTAL_ECH', '0'))}`,
            paymentDue.TOTAL_A_PAYER = `${getNumberWithSpaces(get(paymentDue, 'TOTAL_A_PAYER', '0'))}`,
            paymentDue.MONTANT_PRET = `${getNumberWithSpaces(get(paymentDue, 'MONTANT_PRET', '0'))}`,
            paymentDue.TOTAL_CREDIT_A_PAYER = `${getNumberWithSpaces(get(paymentDue, 'TOTAL_CREDIT_A_PAYER', '0'))}`

            let message = getInterpolatedMsg(template, paymentDue, 'email');
            const splitted = message.split(/\n/);
            message = splitted.filter((elt: any) => !!elt);

            const templateMail = compile(mailTemplate.toString());
            const htmlBody = templateMail({ mailContent: message, date: `${moment().format('DD MMMM YYYY')}` });
            const subjectLang = (get(paymentDue, 'LANGUE_CLIENT', '') === '001') ? 'subjectFR' : 'subjectEN';

            const notificationElmt: any = {
                message,
                htmlBody,
                language: (get(paymentDue, 'LANGUE_CLIENT', '') === '001') ? 'fr' : 'en',
                status: hasAttachment ? 0 : 100,
                subject: get(template, subjectLang, ''),
                priority: get(template, 'priority'),
                operationCode: OperationCode.CREDIT,
                age: get(paymentDue, 'AGE_CPT', ''),
                type: get(template, 'templateType', ''),
                format: 200,
                dates: { createdAt: moment().valueOf() },
                num_cpt: get(paymentDue, 'COMPTE_CLI', ''),
                num_ech: get(paymentDue, 'NUM_ECHEANCE', ''),
                clientCode: get(paymentDue, 'CODE_CLIENT', ''),
                num_dossier: get(paymentDue, 'NUM_DOSSIER', ''),
                clientName: get(paymentDue, 'NOM_DU_CLIENT', ''),
                email: get(paymentDue, 'EMAIL_CLIENT', ''),
                clientProfile: get(template, 'clientProfile', ''),
                telephone: get(paymentDue, 'TELEPHONE_CLIENT', ''),
                source: config.get('source')
            };

            if (template.templateType === 4 && firstReminderDate) {
                notificationElmt.first_reminder_date = firstReminderDate;
            }

            notifications.push(notificationElmt);
        }

        notifications = notifications.filter(notification => notification.email);

        if (config.get('notificationSettings.activateLimitEmailNumber') === true) {
            notifications = notifications.slice(0, config.get('notificationSettings.emailLimit'));
        }
        if (config.get('activateEmailAndPhoneReplacement')) {
            notifications.map((notification: Notification) => {
                notification.email = contactsEmail[commonService.getRandomInt(0, (contactsSMS.length - 1))];
            });
        }

        return notifications;
    },

    getCLientProfileFilter: (clientProfile: number) => {

        if (!clientProfile) { return; }

        const profileFilter = clientProfile === ClientProfile.CORPORATE
            ? '600'
            : { $ne: '600' };

        return profileFilter;
    },

    splitArrayIntoChunks(arr: any[], lenght: number) {
        const chunks = [];
        let i = 0;
        const n = arr.length;
        while (i < n) { chunks.push(arr.slice(i, i += lenght)); }
        return chunks;
    },

    generateHmtl: (notification: any, templateData: any) => {

        const data = getReminderLeterPDFData(notification);

        const template = compile(`${templateData}`);

        return template(data);
    },

    getDaysSinceLastTFJ: (startDate: any, endDate: any) => {

        const dates = [];

        const currDate = momentT(startDate).startOf('day');
        const lastDate = momentT(endDate).startOf('day');

        while (currDate.add(1, 'days').diff(lastDate) < 0) {
            dates.push(currDate.clone().toDate());
        }

        return dates;

    },
}

const interpolate = (template: any, data: any, opts: any): any => {
    let regex: any;
    let lDel: any;
    let rDel: any;
    let delLen: any;
    let lDelLen: any;
    let delimiter: any;
    // For escaping strings to go in regex
    const regexEscape = /([$\^\\\/()|?+*\[\]{}.\-])/g;

    opts = opts || {};

    delimiter = opts.delimiter || '{{}}';
    delLen = delimiter.length;
    lDelLen = Math.ceil(delLen / 2);
    // escape delimiters for regex
    lDel = delimiter.substr(0, lDelLen).replace(regexEscape, '\\$1');
    rDel = delimiter.substr(lDelLen, delLen).replace(regexEscape, '\\$1') || lDel;

    // construct the new regex
    regex = new RegExp(lDel + '[^' + lDel + rDel + ']+' + rDel, 'g');

    return template.replace(regex, (placeholder: any) => {
        const key = placeholder.slice(lDelLen, -lDelLen);
        const keyParts = key.split('.');
        let val: any;
        let i = 0;
        const len = keyParts.length;

        if (key in data) {
            // need to be backwards compatible with "flattened" data.
            val = data[key];
        }
        else {
            // look up the chain
            val = data;
            for (; i < len; i++) {
                if (keyParts[i] in val) {
                    val = val[keyParts[i]];
                } else {
                    return placeholder;
                }
            }
        }
        return val;
    });
};

const getInterpolatedMsg = (template: any, paymentDue: any, type?: string) => {

    const pathTextLang = (get(paymentDue, 'LANGUE_CLIENT', '') === '001') ? `${type}.frenchText` : `${type}.englishText`;
    const templateText = get(template, pathTextLang, '');
    return interpolate(templateText, {
        CIVILITE_CLIENT: `${get(paymentDue, 'CIVILITE_CLIENT', '')}`,
        CODE_CLIENT: `${get(paymentDue, 'CODE_CLIENT', '')}`,
        NUM_DOSSIER: `${get(paymentDue, 'NUM_DOSSIER', '')}`,
        DATE_ECHEANCE: `${moment(paymentDue.DATE_APPEL, '').format('DD/MM/YY')}`,
        CODE_AGE: `${get(paymentDue, 'CODE_AGE', '')}`,
        NUM_ECHEANCE: `${get(paymentDue, 'NUM_ECHEANCE', '')}`,
        STATUT: `${get(paymentDue, 'STATUT', '')}`,
        NOM_CLIENT: `${get(paymentDue, 'NOM_CLIENT', '')}`,
        PRENOM_CLIENT: `${get(paymentDue, 'PRENOM_CLIENT', '')}`,
        SEXE_CLIENT: `${get(paymentDue, 'SEXE_CLIENT', '')}`,
        NOM_DU_CLIENT: `${get(paymentDue, 'NOM_DU_CLIENT', '')}`,
        TYPE_CLIENT: `${get(paymentDue, 'TYPE_CLIENT', '')}`,
        PROFIL_CLIENT: `${get(paymentDue, 'PROFIL_CLIENT', '')}`,
        CODE_PROFIL_CLIENT: `${get(paymentDue, 'CODE_PROFIL_CLIENT', '')}`,
        TELEPHONE_CLIENT: `${get(paymentDue, 'TELEPHONE_CLIENT', '')}`,
        EMAIL_CLIENT: `${get(paymentDue, 'EMAIL_CLIENT', '')}`,
        CODE_GESTIONNAIRE: `${get(paymentDue, 'CODE_GESTIONNAIRE', '')}`,
        NOM_GESTIONNAIRE: `${get(paymentDue, 'NOM_GESTIONNAIRE', '')}`,
        EMAIL_GESTIONNAIRE: `${get(paymentDue, 'EMAIL_GESTIONNAIRE', '')}`,
        TOTAL_ECH: `${(get(paymentDue, 'TOTAL_ECH', '0'))}`,
        TOTAL_A_PAYER: `${(get(paymentDue, 'TOTAL_A_PAYER', '0'))}`,
        MONTANT_PRET: `${(get(paymentDue, 'MONTANT_PRET', '0'))}`,
        CODE_AGE_COMPTE: `${get(paymentDue, 'CODE_AGE_COMPTE', '')}`,
        NUM_COMPTE_CLIENT: `${get(paymentDue, 'COMPTE_CLI', '')}`,
        CLE_COMPTE: `${get(paymentDue, 'CLE_CPT', '')}`,
        DATE_PRE_LETTRE: `${get(paymentDue, 'first_reminder_date', '')}`,
        TOTAL_CREDIT_A_PAYER: `${(get(paymentDue, 'TOTAL_CREDIT_A_PAYER', ''))}`,
        MIMP: `${(get(paymentDue, 'MIMP', ''))}`
    }, {});
};

const checkIsCongonianNumber = (number: any) => {
    number = `${number}`.replace('+', '');
    if (`${number}`.length !== 12 || `${number}`.slice(0, 3) !== "242") { return false; }
    return true;
};

const getReminderLeterPDFData = (notication: any) => {
    const data: any = {};
    data.letter_date = moment().format('DD/MM/YYYY');
    data.object = `${get(notication, 'object', '')}`;
    data.client_name = `${get(notication, 'clientName', '')}`;
    data.client_email = `${get(notication, 'clientEmail')}`;
    data.client_phone = `${get(notication, 'clientPhone', 'N/A')}`;
    data.num_dossier = `${get(notication, 'num_dossier')}`;
    data.message = `${get(notication, 'message')}`;
    return data;
};

const getFirstReminderDate = async (paymentDue: any): Promise<any> => {

    const firstReminder = await successfulCreditNotifysCollection.getSuccessfulCreditNotificationBy(
        {
            clientCode: get(paymentDue, 'CODE_CLIENT', ''),
            num_dossier: get(paymentDue, 'NUM_DOSSIER', ''),
            num_ech: get(paymentDue, 'NUM_ECHEANCE', ''),
            num_cpt: get(paymentDue, 'COMPTE_CLI', ''),
            type: 3
        }
    );

    if (!firstReminder || !get(firstReminder, 'dates.sentAt')) { return null; }

    return get(firstReminder, 'dates.sentAt');
};

const getNumberWithSpaces = (x: any) => {
    if (!x) { return '0' }
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
};