import { paymentDuesCollection } from '../collections/payment-dues.collection';
import { notificationPaymentDuesCollection } from '../collections/notification-payment-dues.collection';
import { commonService } from './common.service';
import { InsertWriteOpResult } from 'mongodb';
import * as http from 'request-promise';
import { logger } from '../winston';
import { config } from '../config';
import * as moment from 'moment';
moment.locale('fr');

const cbsApiUrl = `${config.get('cbsApiUrl')}/api/v1`;

export const paymentDuesService = {

    updateReportingPaymentDues: async () => {
        const start = moment().subtract(1, 'month').startOf('month').valueOf();
        const end = moment().subtract(1, 'month').endOf('month').valueOf();
        const qs = { start, end };

        try {
            const options = {
                method: 'GET',
                uri: `${cbsApiUrl}/payment-dues`,
                qs,
                json: true
            };

            logger.info(`fetch payment dues from cbs \n${JSON.stringify(options)}`);
            const paymentDues: any[] = await http(options);
            logger.info(`${paymentDues.length} payment dues retreived`);

            await paymentDuesCollection.dropCollection();
            logger.info(`old payment-dues collection dropped`);

            const insertResult: InsertWriteOpResult<any> = await paymentDuesCollection.insertPaymentDues(paymentDues);
            logger.info(`${insertResult.insertedCount} payment dues inserted`);

        } catch (error) {
            logger.error(`update payment dues failed \n${error.name} \n${error.stack}`);
            return error;
        }

    },

    updateNotificationsPaymentDues: async (dayFilter: any) => {
        const start = moment(dayFilter).subtract(30, 'days').startOf('day').valueOf();
        const end = moment(dayFilter).add(15, 'days').endOf('day').valueOf();
        const qs = { start, end };

        try {
            const options = {
                method: 'GET',
                uri: `${cbsApiUrl}/payment-dues`,
                qs,
                json: true
            };

            logger.info(`fetch notication payment dues from cbs \n${JSON.stringify(options)}`);
            const paymentDues: any[] = await http(options);
            logger.info(`${paymentDues.length} notification payment dues retreived`);

            await notificationPaymentDuesCollection.dropCollection();
            logger.info(`old notification-payment-dues collection dropped`);

            const insertResult: InsertWriteOpResult<any> = await notificationPaymentDuesCollection.insertNotificationPaymentDues(paymentDues);
            logger.info(`${insertResult.insertedCount} notification payment dues inserted`);

        } catch (error) {
            logger.error(`update notification's payment dues failed \n${error.name} \n${error.stack}`);
            return error;
        }

    },

    getNoticationsStatus: async (filters: any) => {
        try {
            commonService.parseNumberFields(filters);

            let { start, end } = filters;
            start = start && moment(start).isValid() ? moment(start).startOf('day').valueOf() : moment().startOf('month').valueOf();
            end = end && moment(end).isValid() ? moment(end).endOf('day').valueOf() : moment().endOf('month').valueOf();

            return await paymentDuesCollection.getUsersContactStatus(start, end);
        } catch (error) {
            logger.error(`failed to fetch notifications statistics \n${error.name} \n${error.stack}`);
            return error;
        }

    }

}
