import { notificationPaymentDuesCollection } from '../collections/notification-payment-dues.collection';
import { unreachablesClientCollection } from '../collections/unreachable-client.collection';
import { unreachablesClientService } from './unreachable-client.service';
import { notifysCollection } from '../collections/notify.collection';
import { ClientProfile } from '../enums/client-profile.enum';
import { paymentDuesService } from './payment-dues.service';
import { generatePdfService } from './generate-pdf.service';
import { templatesService } from './templates.service';
import { tfjDatesService } from './tfj-dates.service';
import { ReminderType } from '../enums/reminder.enum';
import { helper } from './helpers/notify.helper';
import { commonService } from './common.service';
import { InsertWriteOpResult } from 'mongodb';
import momentT = require('moment-timezone');
import * as http from 'request-promise';
import { get, isEmpty } from 'lodash';
import { logger } from '../winston';
import { config } from '../config';
import moment = require('moment');

export const notifyService = {

    generateNotifications: async () => {

        // Get last TFJ execution date
        const { lastTfjDate } = await tfjDatesService.getLastTfjDate();
        const currDate = moment().valueOf();

        if (moment(currDate).diff(lastTfjDate, 'minutes') <= 200) {
            return await tfjDatesService.updateLastTfjDate(currDate);
        }

        await callGenerateOperationsNotification('checkbook', lastTfjDate, currDate);
        await callGenerateOperationsNotification('cart', lastTfjDate, currDate);

        // Get days gap since last TFJ execution
        const unprocessDays: any[] = helper.getDaysSinceLastTFJ(lastTfjDate, currDate) || [];

        // set last last TFJ execution date to current date
        await tfjDatesService.updateLastTfjDate(currDate);

        // Add current date to days list of generation process
        unprocessDays.push(currDate);

        // Generation notifications
        await startGenerateNoticationsPorcess(unprocessDays);

        // insert unreachable clients
        const table = await unreachablesClientService.getUnreachableClients();
        await unreachablesClientCollection.insertUnreachables(table);

    },

    checkAndSendCreditsNotifications: async () => {
        try {
            const notifications = await notifysCollection.getAllCreditsNotications();
            if (notifications.length === 0) { return; }
            for (const notif of notifications) {
                await notifysCollection.deleteCreditsNotificationById(notif._id);
                await notifysCollection.insertSendingNotification(notif);
            }
            logger.info(`insert ${notifications.length} credits notifications in notifications collection for sending `);
        } catch (error) {
            logger.error(`failed to insert credits notifications in notifications collection for sending \n${error.name} \n${error.stack}`);
        }
    },
}

const startGenerateNoticationsPorcess = async (daysFilterArray: any[]): Promise<any> => {

    for (const dayFilter of daysFilterArray) {
        // get insert updated client payment dues
        await paymentDuesService.updateNotificationsPaymentDues(dayFilter);

        // generate notifications to send
        await startGenerationProcess(dayFilter);

        // generate notifications Attachments for frist and second reminder
        await generateNotificationsAttachment();
    }

};

const startGenerationProcess = async (dayFilter: any) => {
    // Set client's Before Deadline Notifications templates
    await setBeforeDeadLineNotifications(ClientProfile.NETWORK, dayFilter);
    await setBeforeDeadLineNotifications(ClientProfile.CORPORATE, dayFilter);

    // Set unpaid Notifications templates
    await setUnpaidNotifications(ClientProfile.NETWORK, dayFilter);
    await setUnpaidNotifications(ClientProfile.CORPORATE, dayFilter);

    // Set First reminder Notifications templates
    await setFirstReminderNotifications(ClientProfile.NETWORK, dayFilter);
    await setFirstReminderNotifications(ClientProfile.CORPORATE, dayFilter);

    // Set Second reminder Notifications templates
    await setSecondReminderNotifications(ClientProfile.NETWORK, dayFilter);
    await setSecondReminderNotifications(ClientProfile.CORPORATE, dayFilter);
}

const setBeforeDeadLineNotifications = async (clientProfile: number, dayFilter?: number) => {
    const client = clientProfile === 100 ? 'CORPORATE' : 'NETWORK';

    try {
        // Get before deadline SMS template
        const smsTemplates: any = await getTemplates(ReminderType.BEFORE_DEADLINE, clientProfile);

        // Get before deadline EMAIL template
        const emailTemplates: any = await getTemplates(ReminderType.BEFORE_DEADLINE, clientProfile);

        // verify if template is enabled
        if (smsTemplates[0].enabled === true) {

            // Get "before deadline" SMS payment dues
            const smsDueDateFilter = helper.getTemplateDateFilter(smsTemplates[0], dayFilter);
            const smsPaymentDues: any[] = await getPaymentDues(smsDueDateFilter, clientProfile);

            logger.info(`${client}'s before deadline sms paymentDues found : ${smsPaymentDues.length}`);
            // generate SMS notifications data
            let smsNotifications: any[] = await helper.generateSMS(smsPaymentDues, smsTemplates[0]) || [];
            smsNotifications = smsNotifications.filter(elt => { return !isEmpty(elt); });

            // insert genarated SMS notications
            if (smsNotifications.length !== 0) {
                const insertSmsResult: InsertWriteOpResult<any> = await notifysCollection.insertCreditsNotifications(smsNotifications);
                logger.info(`${client}'s before deadline SMS notications inserted: ${get(insertSmsResult, 'insertedCount', 0)}`);
            }
        }

        // verify if template is enabled
        if (emailTemplates[0].enabled === true) {

            // Get "before deadline" Email payment dues
            const emailDueDateFilter = helper.getTemplateDateFilter(emailTemplates[0], dayFilter);
            const emailPaymentDues: any[] = await getPaymentDues(emailDueDateFilter, clientProfile);

            logger.info(`${client}'s before deadline email paymentDues found : ${emailPaymentDues.length}`);
            // generate EMAIL notifications data
            let emailNotifications: any = await helper.generateEmails(emailPaymentDues, emailTemplates[0]) || [];
            emailNotifications = emailNotifications.filter((elt: any) => { return !isEmpty(elt); });
            logger.info(`${client}'s before deadline email generated filtred: ${emailNotifications.length}`);

            // insert genarated EMAIL notications
            if (emailNotifications.length !== 0) {
                const insertEmailResult: InsertWriteOpResult<any> = await notifysCollection.insertCreditsNotifications(emailNotifications);
                logger.info(`${client}'s before deadline EMAIL notications inserted: ${get(insertEmailResult, 'insertedCount', 0)}`);
            }
        }

    } catch (error) {
        logger.error(`failed to generate ${client}'s before deadline notifications \n${error.name} \n${error.stack}`);
    }
};

const setUnpaidNotifications = async (clientProfile: number, dayFilter?: number, format?: number) => {
    const client = clientProfile === 100 ? 'CORPORATE' : 'NETWORK';

    try {
        // Get Unpaid SMS template
        const smsTemplates: any = await getTemplates(ReminderType.UNPAID, clientProfile);

        // Get Unpaid EMAIL template
        const emailTemplates: any = await getTemplates(ReminderType.UNPAID, clientProfile);

        // verify if template is enabled
        if (smsTemplates[0].enabled === true) {

            // Get "Unpaid" payment dues
            const smsDueDateFilter = helper.getTemplateDateFilter(smsTemplates[0], dayFilter);
            let smsPaymentDues: any = await getPaymentDues(smsDueDateFilter, clientProfile, 100);

            // remove paymentDues which have MIMP === 0
            smsPaymentDues = smsPaymentDues.filter(elt => { return +elt.MIMP > 0; });
            logger.info(`${client}'s unpaid sms paymentDues found : ${smsPaymentDues.length}`);

            // generate SMS notifications data
            let smsNotifications: any[] = await helper.generateSMS(smsPaymentDues, smsTemplates[0]) || [];
            smsNotifications = smsNotifications.filter(elt => { return !isEmpty(elt); });

            // insert genarated SMS notications
            if (smsNotifications.length !== 0) {
                const insertSmsResult: InsertWriteOpResult<any> = await notifysCollection.insertCreditsNotifications(smsNotifications);
                logger.info(`${client}'s unpaid SMS notications inserted: ${get(insertSmsResult, 'insertedCount', 0)}`);
            }
        }

        // verify if template is enabled
        if (emailTemplates[0].enabled === true) {

            // Get "Unpaid" payment dues
            const emailDueDateFilter = helper.getTemplateDateFilter(emailTemplates[0], dayFilter);
            let emailPaymentDues: any = await getPaymentDues(emailDueDateFilter, clientProfile, 100);

            // remove paymentDues which have MIMP === 0
            emailPaymentDues = emailPaymentDues.filter(elt => { return +elt.MIMP > 0; });
            logger.info(`${client}'s unpaid email paymentDues found : ${emailPaymentDues.length}`);

            // generate EMAIL notifications data
            let emailNotifications = await helper.generateEmails(emailPaymentDues, emailTemplates[0]) || [];
            emailNotifications = emailNotifications.filter((elt: any) => { return !isEmpty(elt); });

            // insert genarated EMAIL notications
            if (emailNotifications.length !== 0) {
                const insertEmailResult: InsertWriteOpResult<any> = await notifysCollection.insertCreditsNotifications(emailNotifications);
                logger.info(`${client}'s unpaid EMAIL notications inserted: ${get(insertEmailResult, 'insertedCount', 0)}`);
            }
        }

    } catch (error) {
        logger.error(`failed to generate ${client}'s unpaid notifications \n${error.name} \n${error.stack}`);
    }
};

const setFirstReminderNotifications = async (clientProfile: number, dayFilter?: number, format?: number) => {
    const client = clientProfile === 100 ? 'CORPORATE' : 'NETWORK';

    try {
        // Get First reminder SMS template
        const smsTemplates: any = await getTemplates(ReminderType.FIRST_REMINDER, clientProfile);

        // Get First reminder EMAIL template
        const emailTemplates: any = await getTemplates(ReminderType.FIRST_REMINDER, clientProfile);

        // verify if template is enabled
        if (smsTemplates[0].enabled === true) {

            // Get First reminder unpaid payment dues
            const smsDueDateFilter = helper.getTemplateDateFilter(smsTemplates[0], dayFilter);
            const smsPaymentDues: any = await getPaymentDues(smsDueDateFilter, clientProfile, 100);
            logger.info(`${client}'s first reminder sms paymentDues found : ${smsPaymentDues.length}`);

            // generate SMS notifications data
            let smsNotifications: any[] = await helper.generateSMS(smsPaymentDues, smsTemplates[0]) || [];
            smsNotifications = smsNotifications.filter(elt => { return !isEmpty(elt); });

            // insert genarated SMS notications
            if (smsNotifications.length !== 0) {
                const insertSmsResult: InsertWriteOpResult<any> = await notifysCollection.insertCreditsNotifications(smsNotifications);
                logger.info(`${client}'s first reminder SMS notications inserted: ${get(insertSmsResult, 'insertedCount', 0)}`);
            }
        }

        // verify if template is enabled
        if (emailTemplates[0].enabled === true) {

            // Get First reminder unpaid  payment dues
            const emailDueDateFilter = helper.getTemplateDateFilter(emailTemplates[0], dayFilter);
            const emailPaymentDues: any = await getPaymentDues(emailDueDateFilter, clientProfile, 100);

            logger.info(`${client}'s first reminder email paymentDues found : ${emailPaymentDues.length}`);
            // generate EMAIL notifications data
            let emailNotifications = await helper.generateEmails(emailPaymentDues, emailTemplates[0], true) || [];
            emailNotifications = emailNotifications.filter(elt => { return !isEmpty(elt); });

            // insert genarated EMAIL notications
            if (emailNotifications.length !== 0) {
                const insertEmailResult: InsertWriteOpResult<any> = await notifysCollection.insertCreditsNotifications(emailNotifications);
                logger.info(`${client}'s first reminder EMAIL notications inserted: ${get(insertEmailResult, 'insertedCount', 0)}`);
            }
        }

    } catch (error) {
        logger.error(`failed to generate ${client}'s first reminder notifications \n${error.name} \n${error.stack}`);
    }
};

const setSecondReminderNotifications = async (clientProfile: number, dayFilter?: number, format?: number) => {
    const client = clientProfile === 100 ? 'CORPORATE' : 'NETWORK';

    try {
        // Get second reminder SMS template
        const smsTemplates: any = await getTemplates(ReminderType.SECOND_REMINDER, clientProfile);

        // Get second reminder EMAIL template
        const emailTemplates: any = await getTemplates(ReminderType.SECOND_REMINDER, clientProfile);

        // verify if template is enabled
        if (smsTemplates[0].enabled === true) {

            // Get Second reminder  payment dues
            const smsDueDateFilter = helper.getTemplateDateFilter(smsTemplates[0], dayFilter);
            const smsPaymentDues: any = await getPaymentDues(smsDueDateFilter, clientProfile, 100);
            logger.info(`${client}'s second reminder sms paymentDues found : ${smsPaymentDues.length}`);

            // generate SMS notifications data
            let smsNotifications: any[] = await helper.generateSMS(smsPaymentDues, smsTemplates[0]) || [];

            smsNotifications = smsNotifications.filter(elt => { return elt.first_reminder_date; });

            // insert genarated SMS notications
            if (smsNotifications.length !== 0) {
                const insertSmsResult: InsertWriteOpResult<any> = await notifysCollection.insertCreditsNotifications(smsNotifications);
                logger.info(`${client}'s second reminder SMS notications inserted: ${get(insertSmsResult, 'insertedCount') || 0}`);
            }
        }

        // verify if template is enabled
        if (emailTemplates[0].enabled === true) {

            // Get Second reminder  payment dues
            const emailDueDateFilter = helper.getTemplateDateFilter(emailTemplates[0], dayFilter);
            const emailPaymentDues: any = await getPaymentDues(emailDueDateFilter, clientProfile, 100);

            logger.info(`${client}'s second reminder email paymentDues found : ${emailPaymentDues.length}`);
            // generate EMAIL notifications data
            let emailNotifications: any[] = await helper.generateEmails(emailPaymentDues, emailTemplates[0], true) || [];

            emailNotifications = emailNotifications.filter(elt => { return elt.first_reminder_date; });

            // insert genarated EMAIL notications
            if (emailNotifications.length !== 0) {
                const insertEmailResult: InsertWriteOpResult<any> = await notifysCollection.insertCreditsNotifications(emailNotifications);
                logger.info(`${client}'s second reminder EMAIL notications inserted: ${get(insertEmailResult, 'insertedCount') || 0}`);
            }
        }

    } catch (error) {
        logger.error(`Failed to generate ${client}'s second reminder notifications \n${error.name} \n${error.stack}`);
    }
};

const getPaymentDues = async (dueDate: any, clientProfile: any, status?: number) => {
    try {
        const profile = clientProfile === 100 ? 'CORPORATE' : 'NETWORK';
        logger.info(`get payment dues with date range : ${JSON.stringify(dueDate)}; and client profile: ${profile}`);
        clientProfile = helper.getCLientProfileFilter(clientProfile);
        return await notificationPaymentDuesCollection.getNotificationPaymentDues({ ...dueDate, clientProfile, status });

    } catch (error) {
        logger.error(`Failed to get payment dues with date range : ${JSON.stringify(dueDate)} and client clientProfile: ${clientProfile} \n${error.stack}`);
        return error;
    }
};

const getTemplates = async (templateType: number, clientProfile: number) => {
    return await templatesService.getTemplates({ templateType, clientProfile });
};

const generateNotificationsAttachment = async () => {
    try {
        let total = await notifysCollection.getTotalEntries('notifications', { status: 0, format: 200 });

        let limit = 0;
        const limitPDF: number = config.get('notificationSettings.limitPdf');
        while (total > 0) {
            limit = (total < limitPDF) ? total : limitPDF;
            const data = await notifysCollection.getNoticationsWithNoLimit({ status: 0, format: 200 }, null, 0, limit);

            await generatePdfService.generateReminderPdfFiles(data);

            total -= limit;
        }
    } catch (error) {
        logger.error(`failed to generate attachments \n${error.stack}`);
        return error;
    }
};

const callGenerateOperationsNotification = async (target: string, lastTfjDate: any, currDate: any) => {

    if (config.get('env') === 'development') { await commonService.timeout(500); }

    try {
        const options = {
            method: 'GET',
            uri: `${config.get('operationsApiUrl')}/api/v1/notify/operations/${target}`,
            qs: { lastTfjDate, currDate },
            json: true
        };
        logger.info(`Attempt to call OPERATIONS API; callGenerateCheckbookNotification options: ${JSON.stringify(options)}`);
        return await http(options);
    } catch (error) {
        logger.error(`failed to reach OPERATIONS callGenerateCheckbookNotification: \n${error.stack}`);
        return error;
    }
};