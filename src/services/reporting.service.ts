import { reportingsCollection } from '../collections/reporting.collection';
import { regionsCollection } from './../collections/regions.collection';
import { notifysCollection } from '../collections/notify.collection';
import { paymentDuesService } from './payment-dues.service';
import { OperationCode } from '../enums/operations.enum';
import { helper } from './helpers/reporting.helper';
import { cbsService } from './cbs.service';
import { isEmpty, get } from 'lodash';
import { compile } from 'handlebars';
import { config } from './../config';
import { logger } from '../winston';
import { readFileSync } from 'fs';
import * as moment from 'moment';

let templateCaeReporting: any;
let templateAgeManagerReporting: any;
let templateRegionManagerReporting: any;
let templateTopManagerReporting: any;

templateCaeReporting = readFileSync(__dirname + '/helpers/templates/cae-reporting-mail.template.html', 'utf8');
templateAgeManagerReporting = readFileSync(__dirname + '/helpers/templates/age-manager-reporting-mail.template.html', 'utf8');
templateRegionManagerReporting = readFileSync(__dirname + '/helpers/templates/region-manager-reporting-mail.template.html', 'utf8');
templateTopManagerReporting = readFileSync(__dirname + '/helpers/templates/top-manager-reporting-mail.template.html', 'utf8');
config.get('replacementEmailTable');
const contacts: any[] = config.get('replacementEmailTable');

export const reportingService = {

    getCaeReporting: async () => {
        try {
            // Get payment dues reporting data
            const paymentDuesData = await reportingsCollection.getCaesReporting();
            const start = moment().subtract(1, 'month').startOf('month').format('DD/MM/YYYY');
            const end = moment().subtract(1, 'month').endOf('month').format('DD/MM/YYYY');

            // Generate payment dues reportings data
            let reportingData = helper.generaCaeTemplateData(paymentDuesData);

            if (config.get('activateEmailAndPhoneReplacement')) {
                reportingData = reportingData.slice(0, contacts.length);
                for (let index = 0; index < contacts.length; index++) {
                    reportingData[index].email = contacts[index];
                };
            }

            const subject = `Rapport hebdomadaire des recouvrements du ${start} au ${end}`;
            const template = compile(templateCaeReporting.toString());
            const notifications = helper.generateNotifications(reportingData, subject, template, start, end)
            await notifysCollection.insertSendingNotifications(notifications);

            logger.info(`cae reporting email successfully saved`);

        } catch (error) {
            logger.error(`failed to save cae reporting email \n${error.name} \n${error.stack}`);
            return error;
        }

    },

    getAgeManagerReporting: async () => {
        try {
            // Get payment dues reporting data
            const paymentDuesData = await reportingsCollection.getCaesReporting();
            const start = moment().subtract(1, 'month').startOf('month').format('DD/MM/YYYY');
            const end = moment().subtract(1, 'month').endOf('month').format('DD/MM/YYYY');

            // Generate payment dues reportings data
            const paymentDuesCaeData = helper.generaCaeTemplateData(paymentDuesData);

            const ages: any[] = await cbsService.getAgeData();

            let paymentDuesAgeData = helper.generaAgeManagerTemplateData(paymentDuesCaeData, ages);

            if (config.get('activateEmailAndPhoneReplacement')) {
                paymentDuesAgeData = paymentDuesAgeData.slice(0, contacts.length);
                for (let index = 0; index < contacts.length; index++) {
                    paymentDuesAgeData[index].age.email_da = contacts[index];
                };
            }

            const template = compile(templateAgeManagerReporting.toString());
            const subject = `Rapport hebdomadaire des recouvrements de l'agence ${get(paymentDuesAgeData[0], 'age.libage')} du ${start} au ${end}`;
            const notifications = helper.generateAgeNotifications(paymentDuesAgeData, subject, template, start, end);
            await notifysCollection.insertSendingNotifications(notifications);

            logger.info(`Age manager reporting email successfully saved`);

        } catch (error) {
            logger.error(`failed to save ages reporting email \n${error.name} \n${error.stack}`);
            return error;
        }
    },

    getRgManagerReporting: async () => {
        try {
            // Get payment dues reporting data
            const paymentDuesData = await reportingsCollection.getCaesReporting();
            const start = moment().subtract(1, 'month').startOf('month').format('DD/MM/YYYY');
            const end = moment().subtract(1, 'month').endOf('month').format('DD/MM/YYYY');

            // Generate payment dues reportings data
            const paymentDuesCaeData = helper.generaCaeTemplateData(paymentDuesData);

            const ages: any[] = await cbsService.getAgeData();
            const regions: any[] = await regionsCollection.getRegionListReporting();

            const paymentDuesAgeData = helper.generaAgeManagerTemplateData(paymentDuesCaeData, ages);

            const paymentDuesRgData = await helper.generaRgManagerTemplateData(paymentDuesAgeData, regions);

            if (config.get('activateEmailAndPhoneReplacement')) {
                for (let index = 0; index < 2; index++) {
                    paymentDuesRgData[index].email_dr = contacts[index];
                    paymentDuesRgData[index].email_dr_adj_1 = contacts[index];
                    paymentDuesRgData[index].email_dr_adj_2 = contacts[index];
                };
            }

            await Promise.all(paymentDuesRgData.map(async (reportingElmt) => {
                let email_dr = '';
                let email_adj = '';

                if (!config.get('activateEmailAndPhoneReplacement')) {
                    // get emails of regional manager and his deputies
                    const emails = getRegionEmailSender(reportingElmt.email_dr, reportingElmt.email_dr_adj_1, reportingElmt.email_dr_adj_2);
                    if (emails.number === 2) {
                        email_dr = emails.email_1;
                        email_adj = emails.email_2;
                    }
                    if (emails.number === 1) { email_dr = emails.email_1; }
                    if (emails.number === 0) { return; }
                    if (config.get('notificationSettings.activatecomexAddressReplacement'))  {
                        email_dr = config.get('notificationSettings.comexAddressReplacement');
                    }
                } else {
                    email_dr = reportingElmt.email_dr;
                }

                const mailContent = helper.generateRgManagerMailContent(reportingElmt, start, end);
                const subject = `Rapport hebdomadaire des recouvrements de la region ${reportingElmt.libreg} du ${start} au ${end}`;
                const template = compile(templateRegionManagerReporting.toString());
                const htmlBody = template(mailContent);

                const notification = helper.generateRegionNotifications(subject, htmlBody, email_dr, email_adj)
                await notifysCollection.insertSendingNotification(notification);
            }));
            logger.info(`regional manager email successfully saved`);

        } catch (error) {
            logger.error(`failed to save regional manager reporting email \n${error.name} \n${error.stack}`);
            return error;
        }
    },

    getTopManagerReporting: async () => {
        try {
            // Get payment dues reporting data
            const paymentDuesData = await reportingsCollection.getCaesReporting();
            const start = moment().subtract(1, 'month').startOf('month').format('DD/MM/YYYY');
            const end = moment().subtract(1, 'month').endOf('month').format('DD/MM/YYYY');

            // Generate payment dues reportings data
            const paymentDuesCaeData = helper.generaCaeTemplateData(paymentDuesData);

            const ages: any[] = await cbsService.getAgeData();
            const regions: any[] = await regionsCollection.getRegionListReporting();
            const siege: any = await regionsCollection.getRegionBy({ code: 'SIEGE' });

            const paymentDuesAgeData = helper.generaAgeManagerTemplateData(paymentDuesCaeData, ages);

            const paymentDuesRgData = await helper.generaRgManagerTemplateData(paymentDuesAgeData, regions);

            const paymentDuesTopData = helper.generaTopManagerTemplateData(paymentDuesRgData);

            // TODO REMOVE AFTER ALL TEST
            if (config.get('notificationSettings.activatecomexAddressReplacement'))  {
                siege.email_dr = config.get('notificationSettings.comexAddressReplacement');
            }

            // Set CAE email to custum contacts

            const mailContent = helper.generateTopManagerMailContent(paymentDuesTopData, start, end);
            const subject = `Rapport mensuel des recouvrements de la BICEC du ${start} au ${end}`;
            const template = compile(templateTopManagerReporting.toString());
            const htmlBody = template(mailContent);

            const notification = helper.generateTopManagerNotifications(siege.email_dr, subject, htmlBody)
            await notifysCollection.insertSendingNotification(notification);

            logger.info(`top management email successfully saved , ${siege.email_dr}`);

        } catch (error) {
            logger.error(`failed to save region manager reporting email \n${error.name} \n${error.stack}`);
            return error;
        }
    },

    startBankReportingProcess: async () => {
        try {

            // Insert updated bank payment dues data
            await paymentDuesService.updateReportingPaymentDues();

            // Send reporting notificaions to caes
            await reportingService.getCaeReporting();

            // Send reporting notificaions to AGE maneger
            await reportingService.getAgeManagerReporting();

            // Send reporting notificaions to Region maneger
            await reportingService.getRgManagerReporting();

            // Send reporting notificaions to top maneger
            await reportingService.getTopManagerReporting();

        } catch (error) {
            logger.error(`failed to send region manager reporting email \n${error.name} \n${error.stack}`);
            return error;
        }
    },

}

const  generateEmailNotification = (reciever: string, subject: string, htmlBody: any, cc?: string) => {

    const notification: any = {
        subject,
        htmlBody,
        priority: 3,
        status: 100,
        format: 200,
        message: '',
        telephone: '',
        email: reciever,
        language:  'fr',
        source: config.get('source'),
        operationCode: OperationCode.CREDIT,
        dates: { createdAt: moment().valueOf() },
    };

    if (cc) { notification.cc = cc; }

    return notification;
};

const getRegionEmailSender = (email_dr?: string, email_dr_adj_1?: string, email_dr_adj_2?: string): any => {

    if (!isEmpty(email_dr) && !isEmpty(email_dr_adj_1) && !isEmpty(email_dr_adj_2)) {
        return { email_1: email_dr, email_2: `${email_dr_adj_1}, ${email_dr_adj_2}`, number: 2 };

    } else if (!isEmpty(email_dr) && !isEmpty(email_dr_adj_1) && isEmpty(email_dr_adj_2)) {
        return { email_1: email_dr, email_2: email_dr_adj_1, number: 2 };

    } else if (!isEmpty(email_dr) && isEmpty(email_dr_adj_1) && !isEmpty(email_dr_adj_2)) {
        return { email_1: email_dr, email_2: email_dr_adj_2, number: 2 };

    } else if (isEmpty(email_dr) && !isEmpty(email_dr_adj_1) && !isEmpty(email_dr_adj_2)) {
        return { email_1: email_dr_adj_1, email_2: email_dr_adj_2, number: 2 };

    } else if (!isEmpty(email_dr) && isEmpty(email_dr_adj_1) && isEmpty(email_dr_adj_2)) {
        return { email_1: email_dr, number: 1 };

    } else if (isEmpty(email_dr) && !isEmpty(email_dr_adj_1) && isEmpty(email_dr_adj_2)) {
        return { email_1: email_dr_adj_1, number: 1 };

    } else if (isEmpty(email_dr) && isEmpty(email_dr_adj_1) && !isEmpty(email_dr_adj_2)) {
        return { email_1: email_dr_adj_2, number: 1 };

    } else if (isEmpty(email_dr) && isEmpty(email_dr_adj_1) && isEmpty(email_dr_adj_2)) {
        return { number: 0 };
    }
};
