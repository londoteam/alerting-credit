import { commonService } from './common.service';
import * as http from 'request-promise';
import { logger } from '../winston';
import { config } from '../config'

const cbsApiUrl = `${config.get('cbsApiUrl')}/api/v1`

export const cbsService = {

    getClientDataByCode: async (code: any) => {

        if (config.get('env') === 'development') { await commonService.timeout(500); }

        try {
            const options = {
                method: 'GET',
                uri: `${cbsApiUrl}/clients/${code}`,
                json: true
            };
            logger.info(`Attempt to call CBS API; getCientDataByCode options: ${JSON.stringify(options)}`);
            return await http(options);
        } catch (error) {
            logger.error(`failed to reach CBS getCientDataByCode: \n${error.stack}`);
            return error;
        }
    },

    updateAgeData: async (age: any) => {

        if (config.get('env') === 'development') { await commonService.timeout(500); }

        try {
            const options = {
                method: 'PUT',
                uri: `${cbsApiUrl}/ages/`,
                body: age,
                json: true
            };
            logger.info(`Attempt to call CBS API; getCientDataByCode options: ${JSON.stringify(options)}`);
            return await http(options);
        } catch (error) {
            logger.error(`failed to reach CBS getgetCientDataByCode: \n${error.stack}`);
            return error;
        }
    },

    getAgeData: async () => {

        if (config.get('env') === 'development') { await commonService.timeout(500); }

        try {
            const options = {
                method: 'GET',
                uri: `${cbsApiUrl}/ages`,
                json: true
            };
            logger.info(`Attempt to call CBS API; getAgeData options: ${JSON.stringify(options)}`);
            return await http(options);
        } catch (error) {
            logger.error(`failed to reach CBS getAgeData: \n${error.stack}`);
            return error;
        }
    },

    getPaymentDues: async (start: string, end: string) => {

        if (config.get('env') === 'development') { await commonService.timeout(500); }

        try {
            const options = {
                method: 'GET',
                uri: `${cbsApiUrl}/ages`,
                json: true
            };
            logger.info(`Attempt to call CBS API; getAgeData options: ${JSON.stringify(options)}`);
            return await http(options);
        } catch (error) {
            logger.error(`failed to reach CBS getAgeData: \n${error.stack}`);
            return error;
        }
    },
};
