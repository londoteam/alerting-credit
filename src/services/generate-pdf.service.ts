import { readFileSync } from 'fs';
import { logger } from '../winston';
import * as http from 'request-promise';

import { notifysCollection } from '../collections/notify.collection';
import { helper } from './helpers/notify.helper';
import { config } from '../config'

export const generatePdfService = {

    generateReminderPdfFiles: async (notifications: any[]) => {

        try {
            const limitPDF: number = config.get('notificationSettings.limitPdf');
            logger.info(`Notifications fetched length: ${notifications.length}`);
            const dataChunks: any[] = helper.splitArrayIntoChunks(notifications, limitPDF);
            logger.info(`Notifications data Chunks length: ${dataChunks.length}`);

            for (const notificationArray of dataChunks) {
                await Promise.all(notificationArray.map(async (notication: any) => {

                    const reminderLetterTemplate = readFileSync(__dirname + `/helpers/templates/reminder-letter.template.html`);

                    const html = helper.generateHmtl(notication, reminderLetterTemplate);

                    const options = {
                        method: 'POST',
                        uri: `${config.get('pdfApiUrl')}/api/v1/generatePdf`,
                        body: { html },
                        json: true
                    }

                    const data = await http(options);
                    await notifysCollection.updateNotificationById(notication._id, { status: 100, attachment: data });
                }));
            }
            logger.info(`Attachment files generated successfully`);
        } catch (error) {
            logger.error(`Error occure while generating pdf,  \n${error.name} \n${error.stack}`);
            return error;
        }

    }

}

