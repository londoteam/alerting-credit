import * as cors from 'cors';
import * as helmet from 'helmet';
import * as morgan from 'morgan';
import { config } from './config';
import * as express from 'express';
import { json, urlencoded } from 'body-parser';
import { logger, morganOption } from './winston';
import * as xmlparser from 'express-xml-bodyparser';


import { notifyController } from './controllers/notify.controller';
import { paymentDuesController } from './controllers/payment-dues.controller';
import { cronService } from './services/cron.service';


const app = express();
app.use(helmet());

// using bodyParser
app.use(urlencoded({ extended: true }));

// using bodyParser to parse JSON bodies into JS objects
app.use(json({ limit: '300mb' }));

// using XML body parser
app.use(xmlparser());

// enabling CORS for all requests
app.use(cors({ origin: true, credentials: true }));

// adding morgan to log HTTP requests
const format = ':remote-addr - ":method :url HTTP/:http-version" :status :response-time ms - :res[content-length] ":referrer" ":user-agent"';
app.use(morgan(format, morganOption));

// Apply middlewares


// Init controllers
notifyController.init(app);
paymentDuesController.init(app);


const main = express().use(config.get('basePath') || '', app);

main.listen(config.get('port'), config.get('host'), async () => {
    logger.info(`server started. Listening on port ${config.get('port')} in "${config.get('env')}" mode`);
    cronService.checkAndSendCreditsNotifications();
});

export default app;